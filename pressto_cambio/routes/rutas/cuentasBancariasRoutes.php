<?php 
	use Illuminate\Support\Facades\Route;

	Route::middleware(['auth','verified','habilitado'])->group(function () {
		Route::get('/ver/cuentas-bancarias', 'CuentasBancariasController@verCuentasBancarias')->name('verCuentasBancarias');
		Route::post('/save/cuenta-bancaria', 'CuentasBancariasController@saveCuentaBancaria')->name('saveCuentaBancaria');
		Route::post('/update/cuenta-bancaria/{i}', 'CuentasBancariasController@updateCuentaBancaria')->name('updateCuentaBancaria');
		Route::get('/buscar/cuentas-bancarias/{i}', 'CuentasBancariasController@findCuentaBancaria')->name('findCuentaBancaria');
		Route::post('/delete/cuentas-bancarias/{i}', 'CuentasBancariasController@deleteCuentaBancaria')->name('deleteCuentaBancaria');
	});
?>