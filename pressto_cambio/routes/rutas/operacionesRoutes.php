<?php

	use Illuminate\Support\Facades\Route;

	Route::middleware(['auth','usuario','verified','habilitado'])->group(function () {
		Route::get('/realizar/operacion', 'OperacionesController@realizarOperacion')->name('realizarOperacion');
		Route::post('/save/Operacion', 'OperacionesController@saveOperacion')->name('saveOperacion');
	});

	Route::middleware(['auth','admin','verified','habilitado'])->group(function () {
		Route::post('/update/operacion/{id_operacion}', 'OperacionesController@updateOperacion')->name('updateOperacion');
		Route::post('/delete/operacion/{id_operacion}', 'OperacionesController@deleteOperacion')->name('deleteOperacion');
		Route::get('/ver/historial/operaciones', 'OperacionesController@verTodasLasOperaciones')->name('verTodasLasOperaciones');
	});

	Route::middleware(['auth','verified','habilitado'])->group(function () {
		Route::get('/find/operacion/{i}', 'OperacionesController@findOperacion')->name('findOperacion');
	});

	//TODO: esto no iria aca, va en el controlador de vouchers.
	Route::middleware(['auth','verified','admin'])->group(function () {
		Route::post('/save/voucher', 'OperacionesController@saveVoucher')->name('saveVoucher');
	});
?>