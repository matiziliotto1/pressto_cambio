<?php
	use Illuminate\Support\Facades\Route;
	
	Route::middleware(['auth','verified','habilitado'])->group(function () {
		//TODO: este url se usa para obtener el ultimo tipo de cambio...
		Route::get('/find/last/tipo-de-cambio', 'TiposDeCambioController@findLastTipoDeCambio')->name('findLastTipoDeCambio');
		//TODO: este url se usan para las graficas...
		Route::get('/listarTiposDeCambio', 'TiposDeCambioController@listarTiposDeCambio')->name('listarTiposDeCambio');
	});

	Route::middleware(['auth','admin','verified','habilitado'])->group(function (){
		Route::get('/ver/tipos-de-cambio', 'TiposDeCambioController@verTiposDeCambio')->name('verTiposDeCambio');
		Route::post('/save/tipo-de-cambio', 'TiposDeCambioController@saveTipoDeCambio')->name('saveTipoDeCambio');
	});
?>