<?php
    use Illuminate\Support\Facades\Route;

    Route::middleware(['auth','habilitado'])->group(function () {
        Route::get('/crear/perfil', 'PerfilesController@crearPerfil')->name('crearPerfil');
        Route::post('/create/perfil/personal', 'PerfilesController@createPerfilPersonal')->name('createPerfilPersonal');
        Route::post('/create/perfil/empresa', 'PerfilesController@createPerfilEmpresa')->name('createPerfilEmpresa');

        Route::post('/update/perfil/personal/{id_usuario_personal}', 'PerfilesController@updatePerfilPersonal')->name('updatePerfilPersonal');
        Route::post('/update/perfil/empresa/{id_usuario_empresa}', 'PerfilesController@updatePerfilEmpresa')->name('updatePerfilEmpresa');
        
        Route::post('/change/perfil', 'PerfilesController@changePerfilActivo')->name('changePerfilActivo');
        Route::get('/get/perfiles', 'PerfilesController@getPerfiles')->name('getPerfiles');
        Route::get('/ver/perfil', 'PerfilesController@verPerfil')->name('verPerfil');
    });
?>