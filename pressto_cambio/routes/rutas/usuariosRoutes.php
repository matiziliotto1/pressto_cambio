<?php
	use Illuminate\Support\Facades\Route;

	Route::middleware(['auth','admin','verified','habilitado'])->group(function () {
		Route::get('/ver/usuarios/personales', 'UsuariosController@verUsuariosPersonales')->name('verUsuariosPersonales');
		Route::get('/ver/usuarios/empresas', 'UsuariosController@verUsuariosEmpresas')->name('verUsuariosEmpresas');


		//TODO: a partir de aca no probe ninguna
		Route::get('/pusuario/{id}', 'UsuariosController@pusuario')->name('pusuario');
		Route::get('/dusuario/{id}', 'UsuariosController@dusuario')->name('dusuario');
		Route::get('/ausuario/{id}', 'UsuariosController@ausuario')->name('ausuario');
		Route::get('/putipo/{id}', 'UsuariosController@putipo')->name('putipo');
		Route::get('/uutipo/{id}', 'UsuariosController@uutipo')->name('uutipo');
	});
?>