<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Modelo\Operacion;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::get('/auth/redirect/{provider}', 'Auth\SocialController@redirect');
Route::get('/auth/callback/{provider}', 'Auth\SocialController@callback');

Route::middleware(['auth'])->group(function () {
	Route::get('/', function () { return redirect('home'); });
});

Route::middleware(['auth','habilitado', 'verified'])->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/ver/ayuda', function () { return view('ayuda'); })->name('verAyuda');
});

require_once('rutas/perfilesRoutes.php');
require_once('rutas/tiposDeCambioRoutes.php');
require_once('rutas/cuentasBancariasRoutes.php');
require_once('rutas/operacionesRoutes.php');


require_once('rutas/usuariosRoutes.php');

//TODO: rutas que no estan acomodadas...


Route::middleware(['auth','admin','verified','habilitado'])->group(function () {
	Route::get('/reportesbs', 'Control\Vista@reportesbs')->name('reportesbs');
	Route::get('/reporte', 'Control\Vista@reporte')->name('reporte');
	Route::get('/empresa', 'Control\Vista@empresa')->name('empresa');
});

Route::middleware(['auth','usuario','verified','empresa'])->group(function () {		
	Route::get('/operacion', 'Control\Vista@operacion')->name('operacion');
	Route::get('/historial', 'Control\Vista@historial')->name('historial');
});

// Route::get("/sbs",function(){
// 	$ls = Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("estado",2)->get();
// 	return $ls;
// });

Route::middleware(['auth','verified','admin'])->group(function () {
	// Route::get('/noperacionyvouchers/{id}', function($id){
	// 	$ope = Operacion::select('voucher', 'voucher2', 'voucher3', 'voucher4', 'num_ope', 'num_ope2', 'num_ope3', 'num_ope4')->find($id);

	// 	return view("admin.noperacionesyvouchers", compact("ope"));
	// })->name('noperacionyvouchers');
});