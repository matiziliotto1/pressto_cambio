<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Ubigeo;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/departamento', function () {
	$result = Ubigeo::where("codProvincia", 0)->where("codDistrito", 0)->get();
    return $result;
});
Route::get('/provincia/{departamento}', function ($departamento) {
	$result = Ubigeo::where("dDepartamento", $departamento)->where("codDistrito", 0)->get();
    return $result;
});
Route::get('/distrito/{departamento}/{provincia}', function ($departamento, $provincia) {
	$result = Ubigeo::where("dDepartamento", $departamento)->where("codProvincia", $provincia)->get();
    return $result;
});
