<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVouchersYNumOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers_y_num_operaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_operacion');
            $table->unsignedBigInteger('numero_operacion')->nullable();
            $table->string('voucher')->nullable();

            $table->foreign('id_operacion')
                ->references('id_operacion')
                ->on('operaciones')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers_y_num_operaciones');
    }
}
