<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCuentasBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuentas_bancarias', function (Blueprint $table) {
            $table->id('id_cuenta_bancaria');
            $table->unsignedBigInteger('id_usuario_empresa')->nullable();
            $table->unsignedBigInteger('id_usuario_personal')->nullable();
            $table->unsignedBigInteger('id_banco');
            $table->unsignedBigInteger('id_tipo_cuenta');
            $table->unsignedBigInteger('id_tipo_moneda');

            $table->string('numero_cuenta');
            $table->string('numero_cuenta_cci');
            $table->string('alias');
            $table->boolean('cuenta_propia');
            $table->string('nombre')->nullable();
            $table->unsignedBigInteger('id_tipo_documento')->nullable();
            $table->string('numero_documento')->nullable();
            $table->boolean('autorizo_deposito')->nullable();

            $table->foreign('id_usuario_empresa')
                ->references('id_usuario_empresa')
                ->on('usuarios_empresas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_usuario_personal')
                ->references('id_usuario_personal')
                ->on('usuarios_personales')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            
            $table->foreign('id_banco')
                ->references('id_banco')
                ->on('bancos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_cuenta')
                ->references('id_tipo_cuenta')
                ->on('tipos_cuentas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_moneda')
                ->references('id_tipo_moneda')
                ->on('tipos_monedas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            
            $table->foreign('id_tipo_documento')
                ->references('id_tipo_documento')
                ->on('tipos_documentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuentas_bancarias');
    }
}
