<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTiposMonedasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_monedas', function (Blueprint $table) {
            $table->id('id_tipo_moneda');
            $table->string('nombre');
        });

        $data = [
            ['nombre'=> 'S/ Soles'],
            ['nombre'=> '$ Dólares'],
        ];
        DB::table('tipos_monedas')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_monedas');
    }
}
