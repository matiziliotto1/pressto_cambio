<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTiposCuentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_cuentas', function (Blueprint $table) {
            $table->id('id_tipo_cuenta');
            $table->string('nombre');
        });

        // =====================
        // Al crear la tabla, le inserto los bancos
        // =====================
        $data = [
            ['nombre'=> 'Ahorro'],
            ['nombre'=> 'Corriente'],
            ['nombre'=> 'Cuentas nómina'],
            ['nombre'=> 'Cuenta bancaria para empresas o negocio'],
        ];
        DB::table('tipos_cuentas')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_cuentas');
    }
}
