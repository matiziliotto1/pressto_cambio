<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOcupacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ocupaciones', function (Blueprint $table) {
            $table->id('id_ocupacion');
            $table->string('nombre');
        });

        // =====================
        // Al crear la tabla, le inserto los tipos de documentos
        // =====================
        $data = [
            ['nombre'=> 'Trabajador/a independiente'],
            ['nombre'=> 'Estudiante'],
            ['nombre'=> 'Jubilado/a'],
            ['nombre'=> 'Obrero/a'],
            ['nombre'=> 'Miembro de las fuerzas armadas / miembro del clero'],
            ['nombre'=> 'Dependiente'],
            ['nombre'=> 'Ama de casa'],
            ['nombre'=> 'Empleador/a'],
            ['nombre'=> 'Desempleado'],
            ['nombre'=> 'Trabajador/a del hogar'],
            ['nombre'=> 'No declara'],
        ];
        DB::table('ocupaciones')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ocupaciones');
    }
}
