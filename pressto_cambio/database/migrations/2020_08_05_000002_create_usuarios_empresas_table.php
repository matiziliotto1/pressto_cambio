<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_empresas', function (Blueprint $table) {
            $table->id('id_usuario_empresa');

            $table->unsignedBigInteger('id_user');

            $table->string('ruc');
            $table->string('razon_social');
            $table->string('giro_negocio');
            
            $table->unsignedBigInteger('id_pais');
            $table->unsignedBigInteger('id_direccion_pais');
            $table->unsignedBigInteger('id_departamento');
            $table->unsignedBigInteger('id_provincia');
            $table->unsignedBigInteger('id_distrito');

            $table->string('direccion_fiscal');
            $table->string('email');
            $table->string('telefono');

            $table->string('primer_nombre_rp');
            $table->string('segundo_nombre_rp')->nullable();
            $table->string('primer_apellido_rp');
            $table->string('segundo_apellido_rp')->nullable();
            $table->unsignedBigInteger('id_tipo_documento_rp');
            $table->string('numero_documento_rp');

            $table->string('primer_nombre_c');
            $table->string('segundo_nombre_c')->nullable();
            $table->string('primer_apellido_c');
            $table->string('segundo_apellido_c')->nullable();
            $table->unsignedBigInteger('id_tipo_documento_c');
            $table->string('numero_documento_c');
            $table->string('telefono_c')->nullable();

            $table->boolean('preferencial')->default(false);
            $table->float('pref_compra')->nullable();
            $table->float('pref_venta')->nullable();

            $table->timestamps();

            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_documento_rp')
                ->references('id_tipo_documento')
                ->on('tipos_documentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_documento_c')
                ->references('id_tipo_documento')
                ->on('tipos_documentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_pais')
                ->references('id_pais')
                ->on('paises')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_empresas');
    }
}
