<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id('id_rol');
            $table->string('nombre');
            $table->string('descripcion');
        });

        // =====================
        // Al crear la tabla, le inserto los tipos de roles
        // =====================
        $data = [
            ['nombre'=> 'admin', 'descripcion'=> 'Administrador'],
            ['nombre'=> 'user', 'descripcion'=> 'Usuario'],
        ];
        DB::table('roles')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
