<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTiposDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_documentos', function (Blueprint $table) {
            $table->id('id_tipo_documento');
            $table->string('nombre');
        });
        
        // =====================
        // Al crear la tabla, le inserto los tipos de documentos
        // =====================
        $data = [
            ['nombre'=> 'DNI'],
            ['nombre'=> 'PASAPORTE'],
            ['nombre'=> 'RUC 10'],
            ['nombre'=> 'RUC 20'],
            ['nombre'=> 'Carnet de Extranjería'],
        ];
        DB::table('tipos_documentos')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_documentos');
    }
}
