<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePaisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paises', function (Blueprint $table) {
            $table->id('id_pais');
            $table->string('nombre');
        });

        // =====================
        // Al crear la tabla, le inserto los paises
        // =====================
        $data = [
            ['nombre'=> 'Perú'],
            ['nombre'=> 'Argentina'],
            ['nombre'=> 'Brasil'],
            ['nombre'=> 'Colombia'],
        ];
        DB::table('paises')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paises');
    }
}
