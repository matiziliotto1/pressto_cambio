<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operaciones', function (Blueprint $table) {
            $table->id('id_operacion');
            
            $table->unsignedBigInteger('id_cuenta_bancaria_envio');
            $table->unsignedBigInteger('id_cuenta_bancaria_deposita');
            $table->float('monto');
            $table->unsignedBigInteger('id_tipo_moneda_envia');
            $table->unsignedBigInteger('id_tipo_moneda_recibe');
            $table->unsignedBigInteger('id_cuenta_bancaria_transfiere');
            $table->float('cambio');
            $table->float('taza');
            $table->enum('estado', ['Pendiente', 'Procesando', 'Terminado', 'Cancelado']);
            $table->string('ultimo_usuario');
            $table->timestamps();

            $table->foreign('id_tipo_moneda_envia')
                ->references('id_tipo_moneda')
                ->on('tipos_monedas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_moneda_recibe')
                ->references('id_tipo_moneda')
                ->on('tipos_monedas')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_cuenta_bancaria_envio')
                ->references('id_cuenta_bancaria')
                ->on('cuentas_bancarias')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_cuenta_bancaria_deposita')
                ->references('id_cuenta_bancaria')
                ->on('cuentas_bancarias')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_cuenta_bancaria_transfiere')
                ->references('id_cuenta_bancaria')
                ->on('cuentas_bancarias')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operaciones');
    }
}
