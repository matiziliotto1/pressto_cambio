<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosPersonalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios_personales', function (Blueprint $table) {
            $table->id('id_usuario_personal');

            $table->unsignedBigInteger('id_user');

            $table->unsignedBigInteger('id_pais');
            $table->string('primer_nombre');
            $table->string('segundo_nombre')->nullable();
            $table->string('primer_apellido');
            $table->string('segundo_apellido')->nullable();
            $table->unsignedBigInteger('id_tipo_documento');
            $table->string('numero_documento');
            $table->date('fecha_nacimiento');
            $table->string('telefono')->nullable();
            $table->string('celular1')->nullable();
            $table->string('celular2')->nullable();

            $table->unsignedBigInteger('id_direccion_pais');
            $table->unsignedBigInteger('id_departamento');
            $table->unsignedBigInteger('id_provincia');
            $table->unsignedBigInteger('id_distrito');
            $table->string('direccion');

            $table->unsignedBigInteger('id_ocupacion');
            $table->boolean('persona_expuesta');

            $table->boolean('preferencial')->default(false);
            $table->float('pref_compra')->nullable();
            $table->float('pref_venta')->nullable();

            $table->timestamps();

            $table->foreign('id_user')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_tipo_documento')
                ->references('id_tipo_documento')
                ->on('tipos_documentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_ocupacion')
                ->references('id_ocupacion')
                ->on('ocupaciones')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('id_pais')
                ->references('id_pais')
                ->on('paises')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios_personales');
    }
}
