<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bancos', function (Blueprint $table) {
            $table->id('id_banco');
            $table->string('nombre');
        });

        // =====================
        // Al crear la tabla, le inserto los bancos
        // =====================
        $data = [
            ['nombre'=> 'Banco Azteca'],
            ['nombre'=> 'Banco de Comercio'],
            ['nombre'=> 'Banco de Crédito del Perú | BCP'],
            ['nombre'=> 'Banco de Crédito del Perú | CCI'],
            ['nombre'=> 'Banco Falabella'],
            ['nombre'=> 'Banco GNB Perú'],
            ['nombre'=> 'Banco Interamericano de Finanzas (BanBif)'],
            ['nombre'=> 'Banco Pichincha'],
            ['nombre'=> 'Banco Pichincha | CCI'],
            ['nombre'=> 'Banco Ripley'],
            ['nombre'=> 'Banco Santander Perú'],
            ['nombre'=> 'BBVA'],
            ['nombre'=> 'BBVA | CCI'],
            ['nombre'=> 'Caja Arequipa'],
            ['nombre'=> 'Caja Huancayo'],
            ['nombre'=> 'Caja Ica'],
            ['nombre'=> 'Caja Maynas'],
            ['nombre'=> 'Caja Paita'],
            ['nombre'=> 'Caja Sullana'],
            ['nombre'=> 'Caja Tacna'],
            ['nombre'=> 'Caja Trujillo'],
            ['nombre'=> 'Citibank Perú'],
            ['nombre'=> 'Cusco'],
            ['nombre'=> 'Interbank'],
            ['nombre'=> 'Interbank | CCI'],
            ['nombre'=> 'MiBanco'],
            ['nombre'=> 'Scotiabank Perú'],
        ];
        DB::table('bancos')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bancos');
    }
}
