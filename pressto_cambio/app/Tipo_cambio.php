<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_cambio extends Model
{
    protected $primaryKey = 'id_tipo_cambio';
    public $table = 'tipos_cambios';
}
