<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_cuenta extends Model
{
    protected $primaryKey = 'id_tipo_cuenta';
    public $timestamps = false;
    public $table = 'tipos_cuentas';
}
