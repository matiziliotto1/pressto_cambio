<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta_bancaria extends Model
{
    protected $primaryKey = 'id_cuenta_bancaria';
    public $timestamps = false;
    public $table = 'cuentas_bancarias';

    public function tipo_de_cuenta(){
        return $this->belongsTo('\App\Tipo_cuenta','id_tipo_cuenta');
    }

	public function banco(){
        return $this->belongsTo('\App\Banco','id_banco');
    }

	public function tipo_de_moneda(){
        return $this->belongsTo('\App\Tipo_moneda','id_tipo_moneda');
    }  
}
