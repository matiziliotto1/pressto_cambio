<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher_y_num_operacion extends Model
{
    public $timestamps = false;
    public $table = 'vouchers_y_num_operaciones';
}
