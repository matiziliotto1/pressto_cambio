<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $primaryKey = 'id_pais';
    public $table = 'paises';
    public $timestamps = false;
}
