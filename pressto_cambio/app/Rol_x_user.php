<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol_x_user extends Model
{
    public $table = 'roles_x_users';
    protected $primaryKey = 'id_rol_x_user';
    public $timestamps = false;
}
