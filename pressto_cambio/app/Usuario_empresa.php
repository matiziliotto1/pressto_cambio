<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_empresa extends Model
{
    protected $primaryKey = 'id_usuario_empresa';
    public $table = 'usuarios_empresas';
}
