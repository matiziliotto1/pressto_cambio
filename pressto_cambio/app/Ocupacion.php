<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocupacion extends Model
{
    protected $primaryKey = 'id_ocupacion';
    public $table = 'ocupaciones';
    public $timestamps = false;
}
