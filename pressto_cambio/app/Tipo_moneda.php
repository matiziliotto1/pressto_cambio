<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_moneda extends Model
{
    protected $primaryKey = 'id_tipo_moneda';
    public $timestamps = false;
    public $table = 'tipos_monedas';
}
