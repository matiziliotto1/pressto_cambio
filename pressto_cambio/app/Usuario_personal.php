<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_personal extends Model
{
    protected $primaryKey = 'id_usuario_personal';
    public $table = 'usuarios_personales';
}
