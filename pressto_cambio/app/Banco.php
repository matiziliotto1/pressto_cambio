<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $primaryKey = 'id_banco';
    public $timestamps = false;
}
