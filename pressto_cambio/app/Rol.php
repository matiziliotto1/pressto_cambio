<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rol extends Model
{
    public $table = 'roles';
    protected $primaryKey = 'id_rol';
    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany('App\User', 'roles_x_users', 'id_user', 'id_rol');
    }
}
