<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Operacion extends Model
{
    protected $primaryKey = 'id_operacion';
    public $table = 'operaciones';

    public function cuenta_bancaria_envio(){
        return $this->hasMany('App\Cuenta_bancaria', 'id_cuenta_bancaria' , 'id_cuenta_bancaria_envio');
    }

    public function cuenta_bancaria_deposita(){
        return $this->hasMany('App\Cuenta_bancaria', 'id_cuenta_bancaria', 'id_cuenta_bancaria_deposita');
    }

	public function tipo_moneda_envia(){
        return $this->belongsTo('App\Tipo_moneda', 'id_tipo_moneda_envia', 'id_tipo_moneda');
    }

    public function tipo_moneda_recibe(){
        return $this->belongsTo('App\Tipo_moneda', 'id_tipo_moneda_recibe', 'id_tipo_moneda');
    }

    public function cuenta_bancaria_transfiere(){
        return $this->hasMany('App\Cuenta_bancaria', 'id_cuenta_bancaria', 'id_cuenta_bancaria_transfiere');
    }

    public function get_usuario_personal($id_usuario_personal){
        $usuario_personal = Usuario_personal::find($id_usuario_personal);
        return $usuario_personal;
    }

    public function get_usuario_empresa($id_usuario_empresa){
        $usuario_empresa = Usuario_empresa::find($id_usuario_empresa);
        return $usuario_empresa;
    }
}
