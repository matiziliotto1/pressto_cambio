<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{
    protected $table='ubigeo';
	protected $primaryKey='ubigeo_id';
	public $timestamps=false;
}
