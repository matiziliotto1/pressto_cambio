<?php

namespace App\Http\Middleware;

use Closure;

class Habilitado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->User()->habilitado != 1) {
            \Auth::guard()->logout();
            $request->session()->invalidate();
            return abort(403, 'Usuario inhabilitado.');
        }
        else{
            return $next($request);
        }    
    }
}
