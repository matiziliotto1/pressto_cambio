<?php

namespace App\Http\Middleware;

use Closure;

class Usuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->User()->hasRole('user')) {        	
            return $next($request);
        }
        else{
            abort(403, 'This action is unauthorized.');
        }
    }
}
