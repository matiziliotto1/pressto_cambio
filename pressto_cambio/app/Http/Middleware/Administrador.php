<?php

namespace App\Http\Middleware;

use Closure;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->User()->hasRole('admin')) {
            return $next($request);
        }
        else{
            abort(403, 'This action is unauthorized.');
        }
    }
}
