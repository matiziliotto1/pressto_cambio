<?php

namespace App\Http\Controllers;

use App\Usuario_empresa;
use App\Usuario_personal;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    public function verUsuariosPersonales()
    {
        $usuarios_personales = Usuario_personal::all();

        return $usuarios_personales;
    }

    public function verUsuariosEmpresas()
    {
        $usuarios_empresas = Usuario_empresa::all();

        return $usuarios_empresas;
    }
}



// <?php

// namespace App\Http\Controllers;

// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Auth;
// use App\Http\Controllers\Controller;
// use Exception;
// use App\User;
// use App\Usuario_personal;

// class UsuariosPersonalesController extends Controller
// {
//     //TODO: aca son funcione que qcyo q hacen
//     // function listarUsuarios(){
//     //     $ls=\App\Modelo\Usuario::with("tiposdocumento","pais","paisdireccion","ocupacion","cuenta","user")->where('usuario_id', "!=", 1)->get();
//     //     return view("admin.lusuario",compact("ls"));
//     // }

//     function pusuario($id){
//         $us=\App\User::where("usuario_id",$id)->first();
//         if(!is_null($us)){
//             return view("admin.datos",compact("us"));
//         }else{
//             return "Usuario Eliminado";
//         }
//     }

//     function deleteUsuarioPersonal($id){
//         $usuario_personal = Usuario_personal::find($id);

//         $user = User::find($usuario_personal->id_user)->first();
//         $user->habilitado = 0;

//         $user->save();

//         //TODO: para que retorna este id?..
//         return $id;
//     }

//     //TODO: porque le actualiza la password asi?..
//     function ausuario(Request $request,$id){
//         $us=\App\User::find($id);
//         $us->password=bcrypt($request->ps);
//         $us->save();
//         return "Actualizado Correctamente";
//     }

//     function verTipoCambioPreferencial($id){
//         $usuario_personal = Usuario_personal::where("id_usuario_personal", $id)->first();
//         if(!is_null($usuario_personal)){
//             return view("admin.putipo", compact("usuario_personal"));
//         }else{
//             return response()->json(['message'=>'Usuario eliminado.']);
//         }
//     }

//     function updateTipoCambioPreferencial(Request $request,$id){
//         try {
//             $usuario_personal = Usuario_personal::find($id);
//             $usuario_personal->preferencial = $request->Input("preferencial");
//             $usuario_personal->pref_compra = $request->Input("pref_compra");
//             $usuario_personal->pref_venta = $request->Input("pref_venta");

//             $usuario_personal->save();

//             return response()->json(['success'=> true , 'message'=>'Actualizado Correctamente.']);
//         } catch (Exception $e) {
//             return response()->json(['success'=> false , 'message'=>'Error al actualizar.']);
//         }
//     }
// }

