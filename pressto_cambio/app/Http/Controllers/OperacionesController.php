<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Operacion;
use App\User;
use App\Usuario_empresa;
use App\Usuario_personal;
use App\Cuenta_bancaria;
use App\Tipo_moneda;

class OperacionesController extends Controller
{
    public function realizarOperacion(Request $request)
    {
        $id_perfil = Auth::user()->perfil_activo;
        
        // Perfil personal
        if(Auth::user()->tipo_perfil_activo == 0){
            $tipo_perfil = "id_usuario_personal";
            $perfil = Usuario_personal::where('id_usuario_personal', Auth::user()->perfil_activo)->first();
        }
        else {// Perfil empresa
            $tipo_perfil = "id_usuario_empresa";
            $perfil = Usuario_empresa::where('id_usuario_empresa', Auth::user()->perfil_activo)->first();
        }
        $cuentas_clientes = Cuenta_bancaria::with("tipo_de_cuenta","tipo_de_moneda","banco")->where($tipo_perfil,$id_perfil)->get();
        $cuentas_administrador = Cuenta_bancaria::with("tipo_de_cuenta","tipo_de_moneda","banco")->where("id_usuario_personal",1)->get();
        $tipos_de_monedas = Tipo_moneda::all();
        
        return view('clientes.realizarOperacion', compact('cuentas_clientes', 'tipos_de_monedas', 'cuentas_administrador', 'perfil'));
    }

    public function saveOperacion(Request $request)
    {
        if(!is_null(Auth::User()->perfil_activo) && !is_null(Auth::User()->tipo_perfil_activo)){
            $operacion = new Operacion();
			$operacion->id_cuenta_bancaria_envio = $request->Input("id_cuenta_bancaria_envio");
			$operacion->id_cuenta_bancaria_deposita = $request->Input("id_cuenta_bancaria_deposita");
			$operacion->monto = $request->Input("monto");
			$operacion->id_tipo_moneda_envia = $request->Input("id_tipo_moneda_envia");
			$operacion->id_tipo_moneda_recibe = $request->Input("id_tipo_moneda_recibe");
            $operacion->id_cuenta_bancaria_transfiere = $request->Input("id_cuenta_bancaria_transfiere");

			$operacion->cambio = $request->Input("cambio");
			$operacion->taza = $request->Input("compra");
			$operacion->ultimo_usuario = Auth::user()->username;
			$operacion->save();

            $voucherController = new VouchersController();
            $resultado = $voucherController->saveVouchers($operacion->id_operacion, $request->file('voucher'));

            if($resultado){
                $operacion = $this->obtenerDatosDeOperacion($operacion->id_operacion);

                $voucher_y_num_operaciones_controller = new VouchersController();
                $operacion->vouchers = $voucher_y_num_operaciones_controller->getVouchersYNumOperaciones($operacion->id_operacion);

                $data = array(
                    'operacion' => $operacion,
                );

                $email_admin = User::first()->email;
                $email_user = Auth::user()->email;
                $name_user = Auth::user()->username;
                $vista = 'emails.operacion.nuevaOperacion';

                $operaciones_mail_controller = new OperacionesMailController();
                $envio_mail_ok = $operaciones_mail_controller->sendMailNuevaOperacion($vista, $data, $email_admin, $email_user, $name_user);
            }
            return redirect()->route('realizarOperacion');
        }else{
            return redirect()->route('crearPerfil');
        }
    }

    public function updateOperacion($id_operacion)
    {
        $operacion = Operacion::find($id_operacion);

        switch($operacion->estado){
            case 'Pendiente':
                // De pendiente paso a procesando
                $operacion->estado = 'Procesando';

                $operacion_aux = $this->obtenerDatosDeOperacion($id_operacion);

                $data = array(
                    'operacion' => $operacion_aux,
                    'mensaje' => 'En estos momentos tu operación con Pressto Cambio esta siendo PROCESADA.'
                );

                $email_admin = User::first()->email;
                $email_user = Auth::user()->email;
                $name_user = Auth::user()->username;
                $vista = 'emails.operacion.cambioEstado';

                $operaciones_mail_controller = new OperacionesMailController();
                $envio_mail_ok = $operaciones_mail_controller->sendMailNuevaOperacion($vista, $data, $email_admin, $email_user, $name_user);
                break;
            case 'Procesando':
                //De procesando paso a terminado
                $operacion->estado = 'Terminado';

                $operacion_aux = $this->obtenerDatosDeOperacion($id_operacion);

                $data = array(
                    'operacion' => $operacion_aux,
                    'mensaje' => 'Ha finalizado tu transacción exitosamente.'
                );

                $voucher_y_num_operaciones_controller = new VouchersController();
                $operacion_aux->vouchers = $voucher_y_num_operaciones_controller->getVouchersYNumOperaciones($id_operacion);

                $email_admin = User::first()->email;
                $email_user = Auth::user()->email;
                $name_user = Auth::user()->username;
                $vista = 'emails.operacion.operacionFinalizada';

                $operaciones_mail_controller = new OperacionesMailController();
                $envio_mail_ok = $operaciones_mail_controller->sendMailNuevaOperacion($vista, $data, $email_admin, $email_user, $name_user);
                break;
        }

        $operacion->ultimo_usuario = Auth::user()->username;
        $operacion->save();

        return $operacion;
    }

    private function obtenerDatosDeOperacion($id_operacion)
    {
        $operacion_aux = Operacion::with("cuenta_bancaria_envio", "cuenta_bancaria_envio.banco","cuenta_bancaria_deposita", "cuenta_bancaria_deposita.banco", "tipo_moneda_envia","tipo_moneda_recibe","cuenta_bancaria_transfiere", "cuenta_bancaria_transfiere.banco")->find($id_operacion);

        $usuario_personal = $operacion_aux->get_usuario_personal($operacion_aux->cuenta_bancaria_envio[0]['id_usuario_personal']);
        $usuario_empresa = $operacion_aux->get_usuario_empresa($operacion_aux->cuenta_bancaria_envio[0]['id_usuario_empresa']);

        if (!is_null($usuario_personal)) {
            $operacion_aux->usuario_personal = $usuario_personal;
        }

        if (!is_null($usuario_empresa)) {
            $operacion_aux->usuario_empresa = $usuario_empresa;
        }

        return $operacion_aux;
    }

    public function verTodasLasOperaciones()
    {
        $operaciones_usuarios_personales = Operacion::with("cuenta_bancaria_envio","cuenta_bancaria_deposita","tipo_moneda_envia","tipo_moneda_recibe","cuenta_bancaria_transfiere")->get();
        $operaciones_usuarios_empresas = Operacion::with("cuenta_bancaria_envio","cuenta_bancaria_deposita","tipo_moneda_envia","tipo_moneda_recibe","cuenta_bancaria_transfiere")->get();
        
        foreach($operaciones_usuarios_personales as $operacion){
            $operacion->usuario_personal = $operacion->get_usuario_personal($operacion->cuenta_bancaria_envio[0]['id_usuario_personal']);
        }
        
        foreach($operaciones_usuarios_empresas as $operacion){
            $operacion->usuario_empresa = $operacion->get_usuario_empresa($operacion->cuenta_bancaria_envio[0]['id_usuario_empresa']);
        }

        //Esto es solo una forma de acceder a ciertos valores que se necesitan
        // dd(is_null($operaciones_usuarios_personales[0]->usuario_personal));
        // dd(is_null($operaciones_usuarios_empresas[0]->usuario_empresa));

        //Filtro para no tener operaciones con perfiles nulos.
        $operaciones_usuarios_personales = $operaciones_usuarios_personales->filter(function ($operacion) {
            if (!is_null($operacion->usuario_personal)) {
                return $operacion;
            }
        });

        $operaciones_usuarios_empresas = $operaciones_usuarios_empresas->filter(function ($operacion) {
            if (!is_null($operacion->usuario_empresa)) {
                return $operacion;
            }
        });
        
        $operaciones = $operaciones_usuarios_personales->concat($operaciones_usuarios_empresas);
        $operaciones = $operaciones->sortKeysDesc(); //TODO: hay que ordenarlas por fecha (o id) descendencte.

        return view('verOperaciones', compact("operaciones"));
    }

    public function deleteOperacion($id_operacion)
    {
        $operacion = Operacion::find($id_operacion);
        $operacion->estado = 'Cancelado';
        $operacion->ultimo_usuario = Auth::user()->username;
        $operacion->save();

        return "Operación eliminada correctamente";
    }

    public function findOperacion($id_operacion, Request $request)
    {
        $operacion = Operacion::with("cuenta_bancaria_envio", "cuenta_bancaria_envio.banco","cuenta_bancaria_deposita", "cuenta_bancaria_deposita.banco", "tipo_moneda_envia","tipo_moneda_recibe","cuenta_bancaria_transfiere", "cuenta_bancaria_transfiere.banco")->find($id_operacion);

        if($request->tipo_perfil == 0){
            $operacion->usuario_personal = $operacion->get_usuario_personal($operacion->cuenta_bancaria_envio[0]['id_usuario_personal']);
        }
        else if($request->tipo_perfil == 1){
            $operacion->usuario_empresa = $operacion->get_usuario_empresa($operacion->cuenta_bancaria_envio[0]['id_usuario_empresa']);
        }

        $voucherController = new VouchersController();
        $operacion->vouchers = $voucherController->getVouchers($id_operacion);

        return view("verOperacion", compact("operacion"));
        // return $operacion; //TODO: es mejor devolver esto que la vista...
    }
}
