<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cuenta_bancaria;
use Exception;

class CuentasBancariasController extends Controller
{
    public function saveCuentaBancaria(Request $request){
        // Si no tiene un perfil activo, lo redirecciono a crearse uno
        if(!is_null(Auth::User()->perfil_activo)){

            $cuenta_bancaria = new Cuenta_bancaria();

            if(Auth::User()->tipo_perfil_activo == 0){ // Usuario personal
                $cuenta_bancaria->id_usuario_personal = Auth::User()->perfil_activo;
            }
            else if(Auth::User()->tipo_perfil_activo == 1){ // Usuario empresa
                $cuenta_bancaria->id_usuario_empresa = Auth::User()->perfil_activo;
            }

            $cuenta_bancaria->id_banco = $request->input('id_banco');
            $cuenta_bancaria->id_tipo_cuenta = $request->input('id_tipo_cuenta');
            $cuenta_bancaria->id_tipo_moneda = $request->input('id_tipo_moneda');
            $cuenta_bancaria->numero_cuenta = $request->input('numero_cuenta');
            $cuenta_bancaria->numero_cuenta_cci = $request->input('numero_cuenta_cci');
            $cuenta_bancaria->alias = $request->input('alias');
            $cuenta_bancaria->cuenta_propia = $request->input('cuenta_propia');

            //Agregue estos nuevos para cuando la cuenta no es propia
            $cuenta_bancaria->nombre = $request->input('nombre');
            $cuenta_bancaria->id_tipo_documento = $request->input('id_tipo_documento');
            $cuenta_bancaria->numero_documento = $request->input('numero_documento');

            if($request->input('autorizo_deposito') == "on"){
                $cuenta_bancaria->autorizo_deposito = 1;
            }

            $cuenta_bancaria->save();
            return redirect()->route('verCuentasBancarias');
        }else{
            return redirect('crearPerfil');
        }
    }

    public function updateCuentaBancaria(Request $request,$id){
        // Si no tiene un perfil activo, lo redirecciono a crearse uno
        if(!is_null(Auth::User()->perfil_activo)){
            $cuenta_bancaria = Cuenta_bancaria::find($id);

            $cuenta_bancaria->id_banco = $request->input('id_banco');
            $cuenta_bancaria->id_tipo_cuenta = $request->input('id_tipo_cuenta');
            $cuenta_bancaria->id_tipo_moneda = $request->input('id_tipo_moneda');
            $cuenta_bancaria->numero_cuenta = $request->input('numero_cuenta');
            $cuenta_bancaria->numero_cuenta_cci = $request->input('numero_cuenta_cci');
            $cuenta_bancaria->alias = $request->input('alias');
            $cuenta_bancaria->cuenta_propia = $request->input('cuenta_propia');

            //Agregue estos nuevos para cuando la cuenta no es propia
            $cuenta_bancaria->nombre = $request->input('nombre');
            $cuenta_bancaria->id_tipo_documento = $request->input('id_tipo_documento');
            $cuenta_bancaria->numero_documento = $request->input('numero_documento');

            if($request->input('autorizo_deposito') == "on"){
                $cuenta_bancaria->autorizo_deposito = 1;
            }
            else{
                $cuenta_bancaria->autorizo_deposito = null;
            }

            $cuenta_bancaria->save();
            return redirect()->route('verCuentasBancarias');
        }else{
            return redirect('crearPerfil');
        }
    }

    public function verCuentasBancarias(){
        if(Auth::User()->tipo_perfil_activo == 0){
            $cuentas_bancarias = Cuenta_bancaria::with("tipo_de_cuenta","banco","tipo_de_moneda")->where("id_usuario_personal", Auth::User()->perfil_activo)->get();
        }
        else if(Auth::User()->perfil_activo == 1){
            $cuentas_bancarias = Cuenta_bancaria::with("tipo_de_cuenta","banco","tipo_de_moneda")->where("id_usuario_empresa", Auth::User()->perfil_activo)->get();
        }

        return view('verCuentasBancarias',compact("cuentas_bancarias"));
    }

    public function deleteCuentaBancaria($id){
        try {
            $cuenta_bancaria = Cuenta_bancaria::find($id);
            $cuenta_bancaria->delete();
            return response()->json(['success'=>true , 'message'=>'Cuenta bancaria eliminada correctamente']);
        } catch (Exception $e) {
            return response()->json(['success'=>false , 'message'=>'Ocurrió un error al eliminar la cuenta bancaria']);
        }
    }

    public function findCuentaBancaria($id){
        $cuenta_bancaria = Cuenta_bancaria::find($id);
        return view("verCuentaBancaria",compact("cuenta_bancaria"));
    }
}
