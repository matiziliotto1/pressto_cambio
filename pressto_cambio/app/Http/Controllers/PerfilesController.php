<?php

namespace App\Http\Controllers;

use App\Ocupacion;
use App\Pais;
use App\Tipo_documento;
use App\User;
use App\Usuario_empresa;
use App\Usuario_personal;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PerfilesController extends Controller
{
    public function crearPerfil()
    {
        return view('crearPerfil');
    }

    public function createPerfilPersonal(Request $request)
    {
        try {
            $usuario_personal = new Usuario_personal();

            $usuario_personal->id_user = Auth::user()->id;

            $usuario_personal->primer_nombre = $request->input('primer_nombre');
            $usuario_personal->segundo_nombre = $request->input('segundo_nombre');
            $usuario_personal->primer_apellido = $request->input('primer_apellido');
            $usuario_personal->segundo_apellido = $request->input('segundo_apellido');
            $usuario_personal->id_tipo_documento = $request->input('id_tipo_documento');
            $usuario_personal->numero_documento = $request->input('numero_documento');
            $usuario_personal->fecha_nacimiento = $request->input('fecha_nacimiento');
            $usuario_personal->telefono = $request->input('telefono');
            $usuario_personal->celular1 = $request->input('celular1');
            $usuario_personal->celular2 = $request->input('celular2');

            $usuario_personal->id_pais = $request->input('id_pais');
            $usuario_personal->id_direccion_pais = $request->input('id_pais');
            //TODO: porque dos veces el pais?...
            // $usuario_personal->id_direccion_pais = $request->input('id_direccion_pais');
            $usuario_personal->id_departamento = $request->input('id_departamento');
            $usuario_personal->id_provincia = $request->input('id_provincia');
            $usuario_personal->id_distrito = $request->input('id_distrito');
            $usuario_personal->direccion = $request->input('direccion');

            $usuario_personal->id_ocupacion = $request->input('id_ocupacion');
            $usuario_personal->persona_expuesta = $request->input('persona_expuesta');

            $usuario_personal->save();

            $this->asignarPerfilActivo($usuario_personal->id_usuario_personal, 0);

            return redirect("home");

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function createPerfilEmpresa(Request $request)
    {
        try {
            $usuario_empresa = new Usuario_empresa();

            $usuario_empresa->id_user = Auth::user()->id;

            $usuario_empresa->ruc = $request->input('ruc');
            $usuario_empresa->razon_social = $request->input('razon_social');
            $usuario_empresa->giro_negocio = $request->input('giro_negocio');

            $usuario_empresa->id_pais = $request->input('id_pais');
            $usuario_empresa->id_direccion_pais = $request->input('id_pais');
            //TODO: porque dos veces el pais?...
            // $empresa->id_direccion_pais = $request->input('id_direccion_pais');
            $usuario_empresa->id_departamento = $request->input('id_departamento');
            $usuario_empresa->id_provincia = $request->input('id_provincia');
            $usuario_empresa->id_distrito = $request->input('id_distrito');

            $usuario_empresa->direccion_fiscal = $request->input('direccion_fiscal');
            $usuario_empresa->email = $request->input('email');
            $usuario_empresa->telefono = $request->input('telefono');

            $usuario_empresa->primer_nombre_rp = $request->input('primer_nombre_rp');
            $usuario_empresa->segundo_nombre_rp = $request->input('segundo_nombre_rp');
            $usuario_empresa->primer_apellido_rp = $request->input('primer_apellido_rp');
            $usuario_empresa->segundo_apellido_rp = $request->input('segundo_apellido_rp');
            $usuario_empresa->id_tipo_documento_rp = $request->input('id_tipo_documento_rp');
            $usuario_empresa->numero_documento_rp = $request->input('numero_documento_rp');

            $usuario_empresa->primer_nombre_c = $request->input('primer_nombre_c');
            $usuario_empresa->segundo_nombre_c = $request->input('segundo_nombre_c');
            $usuario_empresa->primer_apellido_c = $request->input('primer_apellido_c');
            $usuario_empresa->segundo_apellido_c = $request->input('segundo_apellido_c');
            $usuario_empresa->id_tipo_documento_c = $request->input('id_tipo_documento_c');
            $usuario_empresa->numero_documento_c = $request->input('numero_documento_c');
            $usuario_empresa->telefono_c = $request->input('telefono_c');

            $usuario_empresa->save();

            $this->asignarPerfilActivo($usuario_empresa->id_usuario_empresa, 1);

            return redirect("home");

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    private function asignarPerfilActivo($perfil_activo, $tipo_perfil_activo)
    {
        if(is_null(Auth::user()->tipo_perfil_activo) && is_null(Auth::user()->perfil_activo)){
            $user = User::find(Auth::user()->id);

            $user->tipo_perfil_activo = $tipo_perfil_activo;
            $user->perfil_activo = $perfil_activo;

            $user->save();
        }
    }

    public function getPerfiles()
    {
        $perfiles_usuario_personal = Usuario_personal::where("id_user", Auth::user()->id)->get();
        $perfiles_usuario_empresa = Usuario_empresa::where("id_user", Auth::user()->id)->get();
        $perfil_activo = Auth::user()->perfil_activo;
        $tipo_perfil_activo = Auth::user()->tipo_perfil_activo;

        return response()->json([
            'cant_perfiles_usuario_personal'=>count($perfiles_usuario_personal), 
            'perfiles_usuario_personal'=> $perfiles_usuario_personal,
            'cant_perfiles_usuario_empresa'=>count($perfiles_usuario_empresa), 
            'perfiles_usuario_empresa'=> $perfiles_usuario_empresa,
            'perfil_activo'=> $perfil_activo,
            'tipo_perfil_activo'=> $tipo_perfil_activo,
            ]);
    }

    public function changePerfilActivo(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);

            $user->tipo_perfil_activo = $request->tipo_perfil_activo;
            $user->perfil_activo = $request->perfil_activo;

            $user->save();

            return response()->json(['success'=>true , 'message'=>'Se cambio el perfil correctamente']);
        } catch (Exception $e) {
            return response()->json(['success'=>false , 'message'=>'Ocurrió un error al cambiar el perfil']);
        }
    }

    public function updatePerfilPersonal(Request $request, $id_usuario_personal)
    {
        try {
            $usuario_personal = Usuario_personal::find($id_usuario_personal);

            $usuario_personal->primer_nombre = $request->input('primer_nombre');
            $usuario_personal->segundo_nombre = $request->input('segundo_nombre');
            $usuario_personal->primer_apellido = $request->input('primer_apellido');
            $usuario_personal->segundo_apellido = $request->input('segundo_apellido');
            $usuario_personal->id_tipo_documento = $request->input('id_tipo_documento');
            $usuario_personal->numero_documento = $request->input('numero_documento');
            $usuario_personal->fecha_nacimiento = $request->input('fecha_nacimiento');
            $usuario_personal->telefono = $request->input('telefono');
            $usuario_personal->celular1 = $request->input('celular1');
            $usuario_personal->celular2 = $request->input('celular2');
            
            $usuario_personal->id_pais = $request->input('id_pais');
            $usuario_personal->id_direccion_pais = $request->input('id_pais');
            //TODO: porque dos veces el pais?...
            // $usuario_personal->id_direccion_pais = $request->input('id_direccion_pais');
            $usuario_personal->id_departamento = $request->input('id_departamento');
            $usuario_personal->id_provincia = $request->input('id_provincia');
            $usuario_personal->id_distrito = $request->input('id_distrito');
            $usuario_personal->direccion = $request->input('direccion');

            $usuario_personal->id_ocupacion = $request->input('id_ocupacion');
            $usuario_personal->persona_expuesta = $request->input('persona_expuesta');

            $usuario_personal->save();

            return response()->json(['success'=>true , 'message'=>'Perfil actualizado correctamente']);

        } catch (Exception $e) {
            return response()->json(['success'=>false , 'message'=>'Ocurrió un error al actualizar el perfil']);
        }
    }

    public function updatePerfilEmpresa(Request $request, $id_usuario_empresa)
    {
        try {
            $usuario_empresa = Usuario_empresa::find($id_usuario_empresa);

            $usuario_empresa->ruc = $request->input('ruc');
            $usuario_empresa->razon_social = $request->input('razon_social');
            $usuario_empresa->giro_negocio = $request->input('giro_negocio');

            $usuario_empresa->id_pais = $request->input('id_pais');
            $usuario_empresa->id_direccion_pais = $request->input('id_pais');
            //TODO: porque dos veces el pais?...
            // $empresa->id_direccion_pais = $request->input('id_direccion_pais');
            $usuario_empresa->id_departamento = $request->input('id_departamento');
            $usuario_empresa->id_provincia = $request->input('id_provincia');
            $usuario_empresa->id_distrito = $request->input('id_distrito');

            $usuario_empresa->direccion_fiscal = $request->input('direccion_fiscal');
            $usuario_empresa->email = $request->input('email');
            $usuario_empresa->telefono = $request->input('telefono');

            $usuario_empresa->primer_nombre_rp = $request->input('primer_nombre_rp');
            $usuario_empresa->segundo_nombre_rp = $request->input('segundo_nombre_rp');
            $usuario_empresa->primer_apellido_rp = $request->input('primer_apellido_rp');
            $usuario_empresa->segundo_apellido_rp = $request->input('segundo_apellido_rp');
            $usuario_empresa->id_tipo_documento_rp = $request->input('id_tipo_documento_rp');
            $usuario_empresa->numero_documento_rp = $request->input('numero_documento_rp');

            $usuario_empresa->primer_nombre_c = $request->input('primer_nombre_c');
            $usuario_empresa->segundo_nombre_c = $request->input('segundo_nombre_c');
            $usuario_empresa->primer_apellido_c = $request->input('primer_apellido_c');
            $usuario_empresa->segundo_apellido_c = $request->input('segundo_apellido_c');
            $usuario_empresa->id_tipo_documento_c = $request->input('id_tipo_documento_c');
            $usuario_empresa->numero_documento_c = $request->input('numero_documento_c');
            $usuario_empresa->telefono_c = $request->input('telefono_c');

            $usuario_empresa->save();

            return response()->json(['success'=>true , 'message'=>'Perfil actualizado correctamente']);

        } catch (Exception $e) {
            return response()->json(['success'=>false , 'message'=>'Ocurrió un error al actualizar el perfil']);
        }
    }

    public function verPerfil(){
        // Si no tiene un perfil activo, lo redirecciono a crearse uno
        if(!is_null(Auth::User()->perfil_activo) && !is_null(Auth::User()->tipo_perfil_activo)){
            $user = Auth::user();

            $tipos_documentos = Tipo_documento::all();
            $ocupaciones = Ocupacion::all();
            $paises = Pais::all();

            if($user->tipo_perfil_activo == 0){
                $perfil_usuario = Usuario_personal::find($user->perfil_activo);
            }
            else if($user->tipo_perfil_activo == 1){
                $perfil_usuario = Usuario_empresa::find($user->perfil_activo);
            }

            return view('verPerfil', compact('perfil_usuario', 'tipos_documentos', 'ocupaciones', 'paises'));
        }else{
            return redirect('crearPerfil');
        }
    }
}
