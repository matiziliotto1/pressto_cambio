<?php

namespace App\Http\Controllers;

use App\Cuenta_bancaria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Operacion;
use Illuminate\Support\Facades\Auth;

class VistasController extends Controller
{
    public function cuentasbancarias(){
        if(Auth::User()->tipo_perfil_activo == 0){
            $cuentas_bancarias = Cuenta_bancaria::with("tipoDeCuenta","bancoAsociado","tipoDeMoneda")->where("id_usuario_personal", Auth::User()->perfil_activo)->get();
        }
        else if(Auth::User()->perfil_activo == 1){
            $cuentas_bancarias = Cuenta_bancaria::with("tipoDeCuenta","bancoAsociado","tipoDeMoneda")->where("id_usuario_empresa", Auth::User()->perfil_activo)->get();
        }
        return view('admin.cuentasbancarias', compact("cuentas_bancarias"));
    }

    public function operacion(){
        return view('admin.operacion');
    }

    public function reportesbs(){
        $ls = Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("estado",2)->get();
        return view('admin.reportesbs',compact("ls"));
    }

    public function reporte(){
        $ls = Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->get();
        return view('admin.historial',compact("ls"));
    }

    public function historial(){
        $ls = Operacion::with("cuentabancariae","cuentabancariat","cuentabancariad","cuentabancariat","monedae","monedad","usuario")->where("usuario_id",Auth::User()->usuario_id)->get();
        return view('admin.historial',compact("ls"));
    }

    public function empresa(){
        return view('admin.empresa');
    }

    public function perfil(){
        // Si no tiene un perfil activo, lo redirecciono a crearse uno
        if(!is_null(Auth::User()->perfil_activo)){
            return view('admin.perfil');
        }else{
            return redirect('personal_o_empresa');
        }
    }
}
