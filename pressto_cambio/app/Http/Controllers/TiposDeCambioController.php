<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Tipo_cambio;
use Exception;

class TiposDeCambioController extends Controller
{
    public function verTiposDeCambio(){
        $tipos_cambio = Tipo_cambio::orderBy('id_tipo_cambio', 'DESC')->get();
        return view('verTipoDeCambio', compact('tipos_cambio'));
    }

    public function saveTipoDeCambio(Request $request){
        if(!is_null(Auth::user()->tipo_perfil_activo) && !is_null(Auth::user()->perfil_activo)){
            try {
                $tipo_de_cambio = new Tipo_cambio();
                $tipo_de_cambio->compra = $request->input('compra');
                $tipo_de_cambio->venta = $request->input('venta');
                $tipo_de_cambio->creado_por = Auth::user()->username;
                $tipo_de_cambio->save();
                return response()->json(['success'=>true , 'message'=>'Tipo de cambio agregado correctamente', 'tipo_de_cambio' => $tipo_de_cambio]);
            } catch (Exception $e) {
                return response()->json(['success'=>false , 'message'=>'Ocurrió un error al agregar el tipo de cambio'.$e->getMessage()]);
            }
        }else{
            return redirect('usuario');
        }
    }

    public function findLastTipoDeCambio() {
        try {
            $tipo_de_cambio = Tipo_cambio::latest()->first();
            return response()->json(['success'=>true , 'tipo_de_cambio'=> $tipo_de_cambio]);
        } catch (Exception $e) {
            return response()->json(['success'=>false , 'message'=>'Ocurrió un error al buscar el ultimo tipo de cambio']);
        }
    }
    
    //TODO: estos se usan para las graficas... YA ANDA
    public function listarTiposDeCambio() {
        $tipo_de_cambio = Tipo_cambio::all();
        return $tipo_de_cambio;
    }
}
