<?php

namespace App\Http\Controllers;

use App\Voucher_y_num_operacion;
use Exception;
use Illuminate\Http\Request;

class VouchersController extends Controller
{
    public function getVouchersYNumOperaciones($id_operacion)
    {
        $vouchers_y_num_operaciones = Voucher_y_num_operacion::where('id_operacion', $id_operacion)->get();

        return $vouchers_y_num_operaciones;
    }

    public function getVouchers($id_operacion)
    {
        $vouchers = Voucher_y_num_operacion::where('id_operacion', $id_operacion)->get();

        return $vouchers;
    }

    public function saveVouchers($id_operacion, $vouchers)
    {
        try {
            // Para ver toda la info que viene en $vouchers: 
            // dd($vouchers);

            // Recorro todos los vouchers adjuntados y voy insertando en la base de datos cada uno, asociandolo a la operacion realizada.
            foreach ($vouchers as $voucher) {
                $vouchers_y_num_operaciones = new Voucher_y_num_operacion();
                $vouchers_y_num_operaciones->id_operacion = $id_operacion;
                $vouchers_y_num_operaciones->voucher = rand(1000000, 100000000).$voucher->getClientOriginalName();
                // Muevo el archivo a la ruta 'assets/voucher/XXXX.xxx'
                $voucher->move(public_path().'/assets/voucher/', $vouchers_y_num_operaciones->voucher);

                $vouchers_y_num_operaciones->save();
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    // TODO: esto nose si sirve de algo, con el de arriba creo que ya anda todo, hasta para el admin.
    public function saveVoucher(Request $request)
    {
        try{
            $operacion = Operacion::find($request->id_operacion);
    
            if($request->hasFile("vou")){
                $operacion->voucher=rand(1000000, 100000000).$request->File('vou')->getClientOriginalName();
                $request->File('vou')->move(public_path().'/assets/voucher/',$operacion->voucher);;
            }

            if($request->hasFile("vou2")){
                $operacion->voucher2=rand(1000000, 100000000).$request->File('vou2')->getClientOriginalName();
                $request->File('vou2')->move(public_path().'/assets/voucher/',$operacion->voucher2);;
            }

            if($request->hasFile("vou3")){
                $operacion->voucher3=rand(1000000, 100000000).$request->File('vou3')->getClientOriginalName();
                $request->File('vou3')->move(public_path().'/assets/voucher/',$operacion->voucher3);;
            }

            if($request->hasFile("vou4")){
                $operacion->vouche4r=rand(1000000, 100000000).$request->File('vou4')->getClientOriginalName();
                $request->File('vou4')->move(public_path().'/assets/voucher/',$operacion->vouche4r);;
            }
    
            $operacion->num_ope = $request->Input("num_ope");
            $operacion->num_ope2 = $request->Input("num_ope2");
            $operacion->num_ope3 = $request->Input("num_ope3");
            $operacion->num_ope4 = $request->Input("num_ope4");
            $operacion->save();

            return response([
                "success"=>true,
                "mensaje"=>"Operación actualizada correctamente"
            ]);
        }catch(Exception $e){
            return response([
                "success"=>false,
                "mensaje"=>"Error al actualizar la operación"
            ]);
        }
    }
}
