<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OperacionesMailController extends Controller
{
    public function sendMailNuevaOperacion($vista, $data, $to_email, $from_email, $from_name)
    {
        try {
            Mail::send($vista, $data, function ($message) use($to_email, $from_email, $from_name) {
                $message->from($from_email, $from_name);
                $message->to($to_email, 'Operaciones Pressto Cambio')->subject('Operaciones Pressto Cambio');
            });
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
