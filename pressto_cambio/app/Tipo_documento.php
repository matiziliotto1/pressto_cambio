<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_documento extends Model
{
    protected $primaryKey = 'id_tipo_documento';
    public $table = 'tipos_documentos';
    public $timestamps = false;
}
