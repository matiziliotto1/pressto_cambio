<footer class="footer">
    <div class="container">
        <div class="row align-items-center flex-row-reverse text-white-50">
            <div class="col-lg-12 col-sm-12 mt-3 mt-lg-0 text-center">
                Copyright © 2020 <a href="#">DLS PERÚ</a>.
            </div>
        </div>
    </div>
</footer>

{{-- @if(! \Auth::User()->hasRole('Administrators'))
    <script type="text/javascript">
        var options = {
            whatsapp: "51 938 123 380", // WhatsApp number
            call_to_action: "Ayuda en línea", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { 
            WhWidgetSendButton.init(host, proto, options); 
            };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    </script>
@endif --}}
