@extends('layouts.app2')

@push('css')
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    Movimientos
@endpush

@push('titulo')
    Movimientos
@endpush

@section('content')

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title"><img src="https://www.dls.com.pe/app/assets/images/movimientos.png">
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table id="example" class="table table-bordered key-buttons text-nowrap" >
                        <thead>
                            <tr>
                                <th class="border-bottom-0 text-center">Fecha</th>
                                <th class="border-bottom-0 text-center">Nombre</th>
                                <th class="border-bottom-0 text-center">Envié</th>
                                <th class="border-bottom-0 text-center">Recibí</th>
                                <th class="border-bottom-0 text-center">T/C</th>
                                <th class="border-bottom-0 text-center">Estado</th>
                                @if(\Auth::User()->hasRole('admin'))
                                    <th class="border-bottom-0 text-center">N° operación y Vouchers</th>
                                @endif
                                <th class="border-bottom-0 text-center">Ver</th>
                                @if(\Auth::User()->hasRole('admin'))
                                    <th class="border-bottom-0 text-center">Cancelar</th>
                                @endif
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($operaciones as $operacion)
                                <tr id="{{$operacion->id_operacion}}">
                                    <td>{{$operacion->created_at}}</td>
                                    @if(isset($operacion->usuario_personal))
                                        <input type="hidden" id="tipo_perfil_{{$operacion->usuario_personal->id_usuario_personal}}" value="0">
                                        <input type="hidden" id="id_perfil_{{$operacion->usuario_personal->id_usuario_personal}}" value="{{$operacion->usuario_personal->id_usuario_personal}}">
                                        <td>{{$operacion->usuario_personal->primer_nombre}} {{$operacion->usuario_personal->segundo_nombre}} {{$operacion->usuario_personal->primer_apellido}} {{$operacion->usuario_personal->segundo_apellido}}</td>
                                    @else
                                        <input type="hidden" id="tipo_perfil_{{$operacion->usuario_empresa->id_usuario_empresa}}" value="1">
                                        <input type="hidden" id="id_perfil_{{$operacion->usuario_empresa->id_usuario_empresa}}" value="{{$operacion->usuario_empresa->id_usuario_empresa}}">
                                        <td>{{$operacion->usuario_empresa->primer_nombre_rp}} {{$operacion->usuario_empresa->segundo_nombre_rp}} {{$operacion->usuario_empresa->primer_apellido_rp}} {{$operacion->usuario_empresa->segundo_apellido_rp}}</td>
                                    @endif
                                    <td>@if($operacion->tipo_moneda_envia->id_tipo_moneda == 1) S/ @else $ @endif  {{number_format($operacion->monto, 2, '.', ',')}}</td>
                                    <td>@if($operacion->tipo_moneda_recibe->id_tipo_moneda == 1)  S/ @else $ @endif {{number_format($operacion->cambio, 2, '.', ',')}}</td>
                                    <td>{{$operacion->taza}}</td>

                                    @if(\Auth::User()->hasRole('user'))
                                        <td>
                                            @if($operacion->estado == "Pendiente")
                                                <button type="button" class="btn btn-info btn-sm"><i class="far fa-bell"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Procesando")
                                                <button type="button" class="btn btn-info btn-sm"><i class="far fa-bell"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Terminado")
                                                <button type="button" class="btn btn-primary btn-sm"><i class="far fa-thumbs-up"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Cancelado")
                                                <button type="button" class="btn btn-secondary btn-sm"><i class="fas fa-exclamation-triangle"></i>{{$operacion->estado}}</button>
                                            @endif
                                        </td>
                                    @endif
                                    @if(\Auth::User()->hasRole('admin'))
                                        <td>
                                            @if($operacion->estado == "Pendiente")
                                                <button type="button" class="btn btn-info btn-sm update-operacion"><i class="far fa-bell"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Procesando")
                                                <button type="button" class="btn btn-info btn-sm update-operacion"><i class="far fa-bell"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Terminado")
                                                <button type="button" class="btn btn-primary btn-sm"><i class="far fa-thumbs-up"></i>{{$operacion->estado}}</button>
                                            @elseif($operacion->estado == "Cancelado")
                                                <button type="button" class="btn btn-secondary btn-sm"><i class="fas fa-exclamation-triangle"></i>{{$operacion->estado}}</button>
                                            @endif
                                        </td>
                                    @endif

                                    @if(\Auth::User()->hasRole('admin'))
                                        <td>
                                            @if( !(isset($operacion->voucher) && isset($operacion->voucher2) && isset($operacion->voucher3) && isset($operacion->voucher4)) )
                                                <center>
                                                    <a href="#" onclick="formularioNumerosOperacionYVouchers({{$operacion->operacion_id}})"><i class="fas fa-upload"></i></a>
                                                </center>
                                            @else
                                                <center>
                                                    Ya cargados
                                                </center>
                                            @endif
                                        </td>
                                    @endif

                                    <td>
                                        <a href="#" class="ver-operacion" data-toggle="modal" data-target="#myModal">Ver</a>
                                    </td>
                                    @if(\Auth::User()->hasRole('admin'))
                                        <td>
                                            @if($operacion->estado != 'Cancelado')
                                                <a href="#" class="delete-operacion">Eliminar</a>
                                            @endif
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="contenido">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>      
    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="contenido2">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateVoucher()">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>      
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    {{-- Script para obtener la operacion que se quiere ver y cargarla en un modal --}}
    <script>
        $(".ver-operacion").on('click', function(){
            var id_operacion = $(this).closest('tr').prop('id');

            let tipo_perfil = $("#tipo_perfil_"+id_operacion).val();
            let id_perfil = $("#id_perfil_"+id_operacion).val();

            token = "{{csrf_token()}}";
            url = "{{ route('findOperacion',":id_operacion") }}";
            url = url.replace(':id_operacion', id_operacion);

            $.ajax({
                type:"get",
                url: url,
                data:{
                    id_operacion: id_operacion,
                    tipo_perfil: tipo_perfil,
                    id_perfil: id_perfil,
                    _token: token,
                },
                success: function(data){
                    $("#contenido").html(data);
                },
            });
        });
    </script>

    <script>
        //Guardo el id de la operacion a la que se le esta agregando vouchers y numeros de operacion.
        var tabla;
        $(document).ready(function() {
            tabla = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'copy', 'excel','pdf', 'colvis' ]
            });
            tabla.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
            tabla.column( '0:visible' ).order( 'desc' ).draw();
        });
    </script>

    @if(\Auth::User()->hasRole('admin'))
        <script>
            $(".update-operacion").on('click', function(){
                var id_operacion = $(this).closest('tr').prop('id');
                alertify.confirm("Procesar operación","¿Esta seguro con procesar esta operación?",
                    function(){
                        let token = "{{csrf_token()}}";
                        let url = "{{ route('updateOperacion',":id_operacion") }}";
                        url = url.replace(':id_operacion', id_operacion);
                        $.ajax({
                            type:"post",
                            url: url,
                            data:{
                                _token: token,
                            },
                            success: function(message){
                                var index = tabla.row($("tr[id=" + i + "]")).index();
                                tabla.cell(index, 5).data(function (data, type, row, meta) {
                                    if (message.estado === 'Pendiente') { return '<button type="button" class="btn btn-info btn-sm update-operacio"><i class="far fa-bell"></i> Pendiente</button>'}
                                    if (message.estado === 'Procesando') { return '<button type="button" class="btn btn-info btn-sm update-operacio"><i class="far fa-bell"></i> Procesando</button>'}
                                    if (message.estado === 'Terminado') { return '<button type="button" class="btn btn-primary btn-sm update-operacio"><i class="far fa-thumbs-up"></i> Terminado</button>'}
                                });
                            },
                        });
                    },
                    function(){
                        alertify.error("Proceso cancelado");
                    }
                );
            });

            $(".delete-operacion").on('click', function(){
                var id_operacion = $(this).closest('tr').prop('id');
                alertify.confirm("Cancelar Transacción","¿Esta seguro que desea cancelar esta transacción? No se podrá revertir esta acción",
                    function(){
                        let token = "{{csrf_token()}}";
                        let url = "{{ route('deleteOperacion',":id_operacion") }}";
                        url = url.replace(':id_operacion', id_operacion);

                        $.ajax({
                            type:"post",
                            url: url,
                            data:{
                                _token: token,
                            },
                            success: function(message){
                                let index = tabla.row($("tr[id=" + i + "]")).index();
                                tabla.cell(index, 7).data(function (data, type, row, meta) {
                                    return '<a href="#" class="btn btn-danger">Anulado</a>';
                                });
                                alertify.success(message);
                            },
                        });
                    },
                    function(){
                        alertify.error("No se canceló la operación");
                    }
                );
            });

            //TODO: estas de aca todavia no estan en funcionamiento
            var id_actual_operacion;
            function formularioNumerosOperacionYVouchers(id){
                id_actual_operacion = id;
                $('#myModal2').modal('toggle');
                $("#contenido2").load("noperacionyvouchers/"+id);
            }

            function updateVoucher() {
                let i = id_actual_operacion;

                var token = '{{csrf_token()}}';

                var fd = new FormData();
                var files = $('#vou')[0].files[0];
                var files2 = $('#vou2')[0].files[0];
                var files3 = $('#vou3')[0].files[0];
                var files4 = $('#vou4')[0].files[0];

                fd.append('id_operacion', i);

                fd.append('vou', files);
                fd.append('vou2', files2);
                fd.append('vou3', files3);
                fd.append('vou4', files4);
                fd.append('num_ope', $("#num_ope").val());
                fd.append('num_ope2', $("#num_ope2").val());
                fd.append('num_ope3', $("#num_ope3").val());
                fd.append('num_ope4', $("#num_ope4").val());
                fd.append('_token', token);

                $.ajax({
                    url: '{{route("saveVoucher")}}',
                    type: 'post',
                    data: fd,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(response){
                        if(response['success']){
                            alertify.success(response['mensaje']);
                            $('#myModal2').modal('hide');
                            $('body').removeClass('modal-open');
                        }
                        else{
                            alertify.error(response['mensaje']);
                        }
                    },
                });
            }
        </script>
    @endif
@endsection