@extends('layouts.app')

@push('css')
    <!-- select2 Plugin -->
    <link href="{{asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet">	
            
    <!--mutipleselect css-->
    <link href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet">
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    Operación
@endpush

@push('titulo')
    Operación
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body" id ="card-body-x">
                    <div class="row">
                        <div class="col-xl-9 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <img src="{{asset('assets/images/boperacion.png')}}">
                                    </div>
                                </div>

                                <div class="card-body">
                                    <form id="form" method="POST" action="{{route('saveOperacion')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="list-group">

                                            {{-- 1era Solapa: Calcula --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title></h5>
												<img src="{{asset('assets/images/calcula.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="card-body overflow-hidden">
                                                            <div class="form-group ">
                                                                <label class="form-label">¿Desde qué cuenta nos envías tu dinero?</label>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" required name="id_cuenta_bancaria_envio">
                                                                    @foreach($cuentas_clientes as $ba)
                                                                        <option value="{{$ba->id_cuenta_bancaria}}">{{$ba->banco->nombre}} | {{$ba->numero_cuenta}} @if(isset($ba->numero_cuenta_cci)) - CCI | {{$ba->numero_cuenta_cci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group ">
                                                                <label class="form-label">¿A qué cuenta deseas que depositemos el dinero?</label>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_cuenta_bancaria_deposita" required>
                                                                    @foreach($cuentas_clientes as $ba)
                                                                        <option value="{{$ba->id_cuenta_bancaria}}">{{$ba->banco->nombre}} | {{$ba->numero_cuenta}} @if(isset($ba->numero_cuenta_cci)) - CCI | {{$ba->numero_cuenta_cci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group ">
                                                                <div class="form-label">
                                                                    Tu nos envías
                                                                </div>

                                                                <div class="custom-controls-stacked">
                                                                    @foreach($tipos_de_monedas as $mo)
                                                                        <label class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" name="id_tipo_moneda_envia" onchange="Calcular()" value="{{$mo->id_tipo_moneda}}" required>
                                                                            <span class="custom-control-label">{{$mo->nombre}}</span>
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="form-label">INGRESA EL MONTO</label>
                                                                <input type="text" class="form-control" id="monto" name="monto" placeholder="Ingrese el monto que nos envías" onkeyup="Calcular();" value="0.00">
                                                            </div>

                                                            <button type="button" class="btn btn-primary btn-block" onclick="Calcular()">CALCULAR</button>
                                                            
                                                            <div id="alerta" class="label label-success" style="visibility: hidden;">
                                                                Si vas a transferir más de U$5 000 (cinco mil dólares americanos), por favor comunícate con nosotros por Whatsapp <a href="https://wa.me/51946091321">AQUÍ</a>. Tenemos un cambio preferencial para ti. 
                                                            </div>

                                                            <br>
                                                            <br>

                                                            <div class="col-md-12 col-xl-12">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="dash-widget text-center">
                                                                            <p>TÚ RECIBES</p>
                                                                            <h3>
                                                                                <span id="reci">
                                                                                </span>

                                                                                <span id="dine">
                                                                                </span>
																			</h3>
                                                                            <p class="mb-0 text-muted">
                                                                                <i class="fas fa-calculator fa-lg bg-warning"></i>
                                                                                Tipo de cambio usado
                                                                                <br>
                                                                                <center><span class="h4 font-weight-bold mb-0" id="tcus"></span></center>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- 2da Solapa: Transfiere --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title> </h5>
												<img src="{{asset('assets/images/transfiere.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="mt-3">
                                                            <div class="form-group">
                                                                <label class="form-label">Elije el banco a donde nos vas a transferir</label>
                                                                {{-- //TODO: esto estaba en el otro operacion.blade.php --}}
                                                                <input type="text" name="id_tipo_moneda_recibe" id="id_tipo_moneda_recibe" hidden>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_cuenta_bancaria_transfiere" id="id_cuenta_bancaria_transfiere" style="width: 100% !important;">
                                                                    @foreach($cuentas_administrador as $ba)
                                                                        <option value="{{$ba->id_cuenta_bancaria}}">{{$ba->banco->nombre}} | {{$ba->numero_cuenta}} @if(isset($ba->numero_cuenta_cci)) - CCI | {{$ba->numero_cuenta_cci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <ul class="list-group">
                                                                <li class="list-group-item">

                                                                </li>
                                                                <li class="list-group-item">
                                                                    TRANSFIÉRENOS
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="btra">
                                                                        </span>

                                                                        <span id="bmon">
                                                                        </span>
                                                                    </span>
                                                                    <br>
                                                                    A NUESTRA CUENTA
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="tcse">
                                                                        </span>

                                                                        <span id="ncse">
                                                                        </span>
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    Y TÚ RECIBIRÁS
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="atra">
                                                                        </span>

                                                                        <span id="amon">
                                                                        </span>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        
                                            {{-- 3era Solapa: Finalizar --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title> </h5>
												<img src="{{asset('assets/images/finalizar.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="float-right">
                                                            <a id="mas_vouchers" style="cursor: pointer;">
                                                                Más vouchers&nbsp; <i class="fas fa-plus-circle"></i>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        <div id="div-adjuntar-vouchers">
                                                            <div class="form-group">
                                                                <div class="form-label">Adjunta el voucher o captura de la transferencia</div>
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input vouchers" name="voucher[]" accept="image/png, .jpeg, .jpg, image/gif">
                                                                    <label class="custom-file-label">Elegir archivo</label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="form-label">Adjunta el voucher o captura de la transferencia</div>
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input vouchers" name="voucher[]" accept="image/png, .jpeg, .jpg, image/gif">
                                                                    <label class="custom-file-label">Elegir archivo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label class="form-label">	
                                                            <img src="{{asset('assets/images/gracias.png')}}">
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="text" id="cambio" name="cambio" hidden="true">
                                        <input type="text" id="compra" name="compra" hidden="true">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-md-12 layout-spacing">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        TIPO DE CAMBIO @if($perfil->preferencial == 1) (preferencial) @endif
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-success text-center">
                                            <div class="card-body">
                                                <p class="mb-0 text-white-50">COMPRA</p>
                                                <i class="fas fa-arrow-alt-circle-up text-green mr-1"></i><h2 class=" mb-0 pure">0.00</h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-info text-centerblanco">
                                            <div class="card-body">
                                                <p class="mb-0 text-whiteblanco-50">VENTA</p>
                                                <i class="fas fa-arrow-alt-circle-down text-red mr-1"></i><h2 class=" mb-0 sale">0.00</h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-warning text-center">
                                            <div class="card-body">
                                                <p class="mb-0 text-white-50">El tipo de cambio variará en:</p><br>
                                                <i class="far fa-clock fa-lg"></i><br> 
                                                <h3 class=" mb-0 cronometro">0.00</h3>
                                                <h5 class=" mb-0">Minutos</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!--Accordion-Wizard-Form js-->
		<script src="{{asset('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
		<script src="{{asset('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
		<script src="{{asset('assets/js/wizard.js')}}"></script>
		
	<!--Select2 js -->
		<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

		<!--MutipleSelect js-->
		<script src="{{asset('assets/plugins/multipleselect/multiple-select.js')}}"></script>
		<script src="{{asset('assets/plugins/multipleselect/multi-select.js')}}"></script>

		<!-- Inline js -->
        <script src="{{asset('assets/js/select2.js')}}"></script>

        {{-- TODO: esta se usara solamente para notificar que caduco la sesion y eso? --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>

        @if($perfil->preferencial == 1)
            @php
                $pref_compra = $perfil->pref_compra;
                $pref_venta = $perfil->pref_venta;
            @endphp
            <script>
                var compra = eval({{$pref_compra}});
                var venta= eval({{$pref_venta}});
                var cam = 0.00;
                var tiempo_cronometro = 900;
                var preferencial = true;
                $(".pure").html(compra);
                $(".sale").html(venta);
            </script>
        @else
            <script>
                var compra = 0.00;
                var venta = 0.00;
                var cam = 0.00;
                var tiempo_cronometro = 900;
                var preferencial = false;
                $(document).ready(function(){
                    token = "{{csrf_token()}}";
                    url = "{{route('findLastTipoDeCambio')}}";

                    $.ajax({
                        type:"GET",
                        url: url,
                        data:{
                            _token: token,
                        },
                        dataType: 'json',

                        success: function(data){
                            if(data['success']){
                                compra = eval(data['tipo_de_cambio'].compra);
                                venta = eval(data['tipo_de_cambio'].venta);
                                $(".pure").html(compra);
                                $(".sale").html(venta);
                            }
                            else{
                                alertify.error(data['message']);
                            }
                        },
                    });
                    function cronometro() {
                        if(tiempo_cronometro === 0){                  
                            let img = "{{asset('assets/images/tiempo.jpg')}}";
                            alertify.alert('', '<img src="'+ img +'">', function(){
                                location.reload();
                            });
                            clearInterval(intervalo);
                        }else{
                            tiempo_cronometro -= 1;
                            var tim=parseInt(tiempo_cronometro/60)+":"+(tiempo_cronometro%60);
                            $('.cronometro').html(tim);
                        }
                    }
                    var intervalo = setInterval(cronometro, 1000);
                });
            </script>
        @endif

        <script>
            $('.vouchers').on('change',function(e){
                //get the file name
                var fileName = e.target.files[0].name;
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>

        <script>
            var cant_vouchers = 1;
            $('#mas_vouchers').on('click',function(e){
                console.log("Entro al click");
                if(cant_vouchers < 4){
                    cant_vouchers += 1;

                    $('#div_vou_'+cant_vouchers).removeClass('d-none');
                }
                else{
                    alertify.set('notifier','position', 'top-right');
                    alertify.error("No se permite subir mas de 4 vouchers");
                }
            })
        </script>

        <script>
            function numberFormat(numero){
                // Variable que contendra el resultado final
                var resultado = "";
        
                // Si el numero empieza por el valor "-" (numero negativo)
                if(numero[0]=="-")
                {
                    // Cogemos el numero eliminando los posibles puntos que tenga, y sin
                    // el signo negativo
                    nuevoNumero=numero.replace(/\,/g,'').substring(1);
                }else{
                    // Cogemos el numero eliminando los posibles puntos que tenga
                    nuevoNumero=numero.replace(/\,/g,'');
                }
        
                // Si tiene decimales, se los quitamos al numero
                if(numero.indexOf(".")>=0)
                    nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf("."));
        
                // Ponemos un punto cada 3 caracteres
                for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++)
                    resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ",": "") + resultado;
        
                // Si tiene decimales, se lo añadimos al numero una vez forateado con 
                // los separadores de miles
                if(numero.indexOf(".")>=0)
                    resultado+=numero.substring(numero.indexOf("."));
        
                if(numero[0]=="-")
                {
                    // Devolvemos el valor añadiendo al inicio el signo negativo
                    return "-"+resultado;
                }else{
                    return resultado;
                }
            }
        </script>
        <script>
            function Calcular(){
                $('#monto').val( numberFormat($("#monto").val()) );
        
                var monto_aux = $('#monto').val();
                var monto = parseFloat($('#monto').val().replace(/,/, ''));
                var tipo_de_moneda = $('input[name=id_tipo_moneda_envia]:checked').val();
                if(tipo_de_moneda==2){
                    if(!preferencial && eval(monto)>=eval(5000)){
                        $("#alerta").css("visibility","visible");
                    }
                    $("#id_tipo_moneda_recibe option").remove();
                    cuentas_dolares.forEach(cuenta=>$("#id_tipo_moneda_recibe").append('<option value="'+cuenta.id_cuenta_bancaria+'">-'+cuenta.tipo_de_cuenta.nombre+' | '+cuenta.tipo_de_moneda.nombre+' | '+cuenta.banco.nombre+'- '+cuenta.numero_cuenta+'</option>'));
                    cambio = (eval(monto)*eval(compra)).toFixed(2);
                    $("#reci").html( "S/" );
                    $("#reci").append( numberFormat(cambio) );
                    $("#dine").html("Soles");
                    $("#cambio").val(cambio);
                    $("#compra").val(compra);
                    $("#tcus").html(compra);
                    $("#atra").html("S/");
                    $("#atra").append(cambio);
                    $("#amon").html("Soles");
                    $("#btra").html("$");
                    $("#btra").append(monto_aux);
                    $("#bmon").html("Dólares");
                    $("#id_tipo_moneda_recibe").val(1);
                }else{
                    $("#id_tipo_moneda_recibe option").remove();
                    cuentas_soles.forEach(cuenta=>$("#id_tipo_moneda_recibe").append('<option value="'+cuenta.id_cuenta_bancaria+'">-'+cuenta.tipo_de_cuenta.nombre+' | '+cuenta.tipo_de_moneda.nombre+' | '+cuenta.banco.nombre+'- '+cuenta.numero_cuenta+'</option>'));
                    cambio = (eval(monto)/eval(venta)).toFixed(2);
                    $("#reci").html("$");
                    $("#reci").append( numberFormat(cambio) );
                    $("#dine").html("Dólares");
                    $("#cambio").val(cambio);
                    $("#tcus").html(venta);
                    $("#atra").html("$");
                    $("#atra").append(cambio);
                    $("#amon").html("Dólares");
                    $("#btra").html("S/");
                    $("#btra").append(monto_aux);
                    $("#bmon").html("Soles");
                    $("#compra").val(venta);
                    $("#id_tipo_moneda_recibe").val(2);
                }
            }
            var cuentas_administrador = @php echo $cuentas_administrador; @endphp;
            var cuentas_soles = cuentas_administrador.filter(cuenta => cuenta.tipo_de_moneda.id_tipo_moneda == 1);
            var cuentas_dolares = cuentas_administrador.filter(cuenta => cuenta.tipo_de_moneda.id_tipo_moneda == 2);
            $(document).ready(function(){
                $("input[name=id_tipo_moneda_envia][value='1']").prop("checked",true);
                function selecte() {
                    var select = $("#id_tipo_moneda_recibe").val();
                    var objeto = cuentas_administrador.find(function(element) {
                        if(element.id_tipo_moneda == select){
                            return element;
                        }else{
                            return false;
                        }
                    });
                    console.log(cuentas_administrador);
                    // TODO: aca todavia no esta andando, ver como acceder a esos datos
                    $("#tcse").html(objeto.banco.nombre+"- "+objeto.tipo_de_cuenta.nombre+" "+objeto.tipo_de_moneda.nombre);
                    $("#ncse").html(objeto.numero_cuenta);
                }
                $("#id_tipo_moneda_recibe").change(function(){
                    selecte();
                });
                selecte();
            });
            function valida(){
                if($("#monto").val()==""){
                    alertify.error("Ingresa el monto");
                }
                if($("#vou").val()==""){
                    alertify.error("Sube una captura del comprobante");
                }
            }
            $(document).on('submit', '#form', function (e) {
                $('#monto').val( parseFloat($('#monto').val().replace(/,/, '')) );
        
                var input = ($("#vou"))[0];
                var file = input.files[0];
                var zz=eval(cam)<1;
                var yy=eval(file.size>8000000);
                if(zz){
                    alertify.error("Aplique la taza de Cambio");
                    e.preventDefault();
                } 
                if(yy){
                    $("#imagen").css("visibility","visible");
                    alertify.error("Error al cargar archivo");
                    e.preventDefault();
                }
                if(!zz&&!yy){
                    $("#global-loader").show();
                }
            });
        </script>
        
@endsection