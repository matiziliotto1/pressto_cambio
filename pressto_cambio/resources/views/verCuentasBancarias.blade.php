@extends('layouts.app')

@push('css')
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    Cuentas Bancarias
@endpush

@push('titulo')
    Bancos
@endpush

@section('content')
    @php 
        $mos=\App\Tipo_moneda::all();
        $ban=\App\Banco::all();
        $tpo=\App\Tipo_cuenta::all();
        $doc=\App\Tipo_documento::all();
    @endphp

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                     <img src="https://www.dls.com.pe/app/assets/images/misbancos.png">
                </div>

                <div class="card-body">
                    <form action="{{route('saveCuentaBancaria')}}" method="POST" id="form_add_account">
                        @csrf
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group ">
                                    <label class="form-label">*Banco</label>
                                    <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_banco">
                                        @foreach($ban as $ba)
                                            <option value="{{$ba->id_banco}}">{{$ba->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group ">
                                    <label class="form-label">*Tipo de cuenta</label>
                                    <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_tipo_cuenta">
                                        @foreach($tpo as $tc)
                                            <option value="{{$tc->id_tipo_cuenta}}">{{$tc->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group ">
                                    <label class="form-label">*Moneda</label>
                                    <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_tipo_moneda">
                                        @foreach($mos as $mo)
                                            <option value="{{$mo->id_tipo_moneda}}">{{$mo->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label">*Número de cuenta</label>
                                    <input type="number" class="form-control" name="numero_cuenta" placeholder="Ingrese el número de cuenta" required>	
                                </div>
                                
                                <div class="form-group">
                                    <label class="form-label">*Número de cuenta CCI</label>
                                    <input type="number" class="form-control" name="numero_cuenta_cci" placeholder="Ingrese el número de cuenta CCI" required>	
                                </div>

                                <div class="form-group">
                                    <label class="form-label">
                                        *Alias de la cuenta 
                                        <span class="form-help bg-primary text-white" data-toggle="popover" data-placement="top"
                                            data-content="<p>Ingresa el alias para identificar esta cuenta, por ejemplo: bcp dolares de mamá</p>
                                            <p class='mb-0'><a href=''>Bcp</a></p>
                                            ">?
                                        </span>   
                                    </label>
                                    <input type="text" class="form-control" name="alias" placeholder="Ingrese un alias">
                                </div>

                                <div class="form-group ">
                                    <div class="form-label">
                                        ¿Esta cuenta es propia?
                                    </div>
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input cuenta_propia"  name="cuenta_propia" value="1" checked>
                                            <span class="custom-control-label">Si</span>
                                        </label>
                                        <label class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input cuenta_propia"  name="cuenta_propia" value="0">
                                            <span class="custom-control-label">No</span>
                                        </label>
                                    </div>
                                </div>

                                <div class="" id="div_cuenta_propia">
                                    <div class="form-group">
                                        <label class="form-label">¿A nombre de quien esta la cuenta?</label>
                                        <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ingresar nombre">
                                    </div>
        
                                    <div class="form-group ">
                                        <label class="form-label">Tipo de documento</label>
                                        <select class="form-control select2 custom-select" name="id_tipo_documento" id="id_tipo_documento" data-placeholder="Elija uno">
                                            <option label="Elija uno"></option>
                                            @foreach($doc as $do)'
												<option value="{{$do->id_tipo_documento}}">{{$do->nombre}}</option>
											@endforeach
                                        </select>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="form-label">*Número documento</label>
                                        <input type="text" class="form-control" name="numero_documento" id="numero_documento" placeholder="Ingrese el número de cuenta">
                                    </div>
        
                                    <div class="form-group">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="autorizo_deposito" id="autorizo_deposito" class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Autorizo que se deposite a esta cuenta</span>
                                        </label>
                                    </div>

                                </div>
                                {{-- TODO: no falta un boton para guardar la cuenta? --}}
                                <div class="card-body">
                                    <div class="btn-list">
                                       <input class="btn btn-primary" type="submit" value="Guardar cuenta" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Listado de cuentas bancarias</h3>
                </div>

                <div class="table-responsive">
                    <table class="table card-table table-vcenter text-nowrap table-striped table-bordered ">
                        <thead>
                            <tr>
                                <th class="text-center">Banco</th>
                                <th class="text-center">Alias</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Moneda</th>
                                <th class="text-center">Nº Cuenta</th>
                                <th class="text-center">Nº Cuenta CCI</th>
                                <th class="text-center">Cta propia</th>
                                <th class="text-center">A nombre de</th>
                               
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($cuentas_bancarias as $l)
                                <tr id="{{$l->id_cuenta_bancaria}}">
                                    {{-- <td><a href="store.html" class="text-inherit">{{$l->banco->nombre}} </a></td> --}}
                                    <th>{{$l->banco->nombre}}</th>
                                    <th>{{$l->alias}}</th>
                                    <th>{{$l->tipo_de_cuenta->nombre}}</th>
                                    <th>{{$l->tipo_de_moneda->nombre}}</th>
                                    <th>{{$l->numero_cuenta}}</th>
                                    <th>{{$l->numero_cuenta_cci}}</th>
                                    <th>
                                        @if($l->cuenta_propia == 1) Si @else No @endif
                                    </th>
                                    <th>{{$l->nombre}}</th>
                                    <th>
                                        <td class="text-right">
                                            <a class="icon" href=""></a>
                                            <a href="{{route('findCuentaBancaria', $l->id_cuenta_bancaria)}}" class="btn btn-primary btn-sm"><i class="fas fa-pencil-alt"></i> Editar</a>
                                            <a class="icon" href="#"></a>
                                            <a href="#" class="btn btn-secondary btn-sm" onclick="remove({{$l->id_cuenta_bancaria}})"><i class="fas fa-trash"></i> Eliminar</a>
                                        </td>
                                    </th>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(".cuenta_propia").on("change", function(){
            let cuenta_propia = $('input[name=cp]:checked').val();

            //Si no es cuenta propia, habilito que ingrese datos de la cuenta NO propia
            if( cuenta_propia == 0 ){
                $("#div_cuenta_propia").removeClass("d-none");

                //Si es una cuenta NO propia, los campos que se habilitan deben ser requeridos
                $("#nombre").attr("required", true);
                $("#tipo_doc").attr("required", true);
                $("#numero_doc").attr("required", true);
                $("#autorizo_deposito").attr("required", true);
            }
            else if (cuenta_propia == 1){
                $("#div_cuenta_propia").addClass("d-none");

                //Si es una cuenta propia, los campos que se deshabilitan les borro el valor actual
                $("#nombre").val("");
                $("#tipo_doc").val("");
                $("#numero_doc").val("");
			    $("#autorizo_deposito").removeAttr("checked");
			    $("#autorizo_deposito").val("");

                //Si es una cuenta propia, los campos que se deshabilitan ya no son requeridos
                $('#nombre').removeAttr("required");
                $('#tipo_doc').removeAttr("required");
                $('#numero_doc').removeAttr("required");
                $('#autorizo_deposito').removeAttr("required");
            }
        })
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
    <script>
        function remove(id_cuenta_bancaria) {
            alertify.confirm("Eliminar Cuenta","¿Esta seguro con eliminar su cuenta?",
                function(){
                    
                    token = "{{csrf_token()}}";
                    url = "{{ route('deleteCuentaBancaria',":id_cuenta_bancaria") }}";
                    url = url.replace(':id_cuenta_bancaria', id_cuenta_bancaria);

                    $.ajax({
                        type:"POST",
                        url: url,
                        data:{
                            _token: token,
                        },
                        dataType: 'json',

                        success: function(data){
                            if(data['success']){
                                alertify.success(data['message']);
                                $("tr[id="+id_cuenta_bancaria+"]").remove();
                            }
                            else{
                                alertify.error(data['message']);
                            }
                        },
                    });
                },
                function(){
                    alertify.error("Eliminación cancelada");
                }
            );
        }
    </script>
@endsection