<div>


	<table class="table">
		<tr>
		  <th colspan="2"><img src="{{asset('assets/images/operacionheader.png')}}"></th>
	    </tr>
		<tr>
			<th>Fecha de Operación</th>
			<td>{{$op->created_at}}</td>
		</tr>
		<tr>
			<th>Fecha de Actualización</th>
			<td>{{$op->updated_at}} {{$op->last_user}}</td>
		</tr>
		<tr>
			<th>Nombres</th>
			<td>{{$op->usuario->primernombre}} {{$op->usuario->segundonombre}}</td>
		</tr>
		<tr>
			<th>Apellidos</th>
			<td>{{$op->usuario->primeroapellido}} {{$op->usuario->segundoapellido}}</td>								
		</tr>
		<tr>
			<th>Cuenta de Envio</th>
			<td>-{{$op->cuentabancariae[0]->banco->nombre}}- {{$op->cuentabancariae[0]->nrocuenta}} @if(isset($op->cuentabancariae[0]->nrocuentacci)) | {{$op->cuentabancariae[0]->nrocuentacci}} @endif</td>
		</tr>
		<tr>
			<th>Monto Enviado</th>
			<td><strong>@if($op->monedae->moneda_id == 1) S/ @else $ @endif {{number_format($op->monto, 2, '.', ',')}}</strong> @if($op->monedae->moneda_id == 1) Soles @else Dólares @endif</td>
		</tr>
		<tr>
			<th>Cuenta Destino</th>
			<td>-{{$op->cuentabancariad[0]->banco->nombre}}- {{$op->cuentabancariad[0]->nrocuenta}} @if(isset($op->cuentabancariad[0]->nrocuentacci)) | {{$op->cuentabancariad[0]->nrocuentacci}} @endif</td>
		</tr>
		<tr>
			<th>Monto Esperado</th>
			<td><strong>@if($op->monedad->moneda_id == 1) S/ @else $ @endif {{number_format($op->cambio, 2, '.', ',')}}</strong> @if($op->monedad->moneda_id == 1) Soles @else Dólares @endif</td>
		</tr>
		<tr>
			<th>Cuenta de Depósito</th>
			<td>-{{$op->cuentabancariat[0]->banco->nombre}}- {{$op->cuentabancariat[0]->nrocuenta}} @if(isset($op->cuentabancariat[0]->nrocuentacci)) | {{$op->cuentabancariat[0]->nrocuentacci}} @endif</td>
		</tr>
		<tr>
			<th colspan="2">
				<img src="{{url('assets/voucher/'.$op->voucher)}}" alt="">
			</th>
		</tr>
		<tr>
			<th colspan="2">
				<img src="{{url('assets/voucher/'.$op->voucher2)}}" alt="">
			</th>
		</tr>
		<tr>
			<th colspan="2">
				<img src="{{url('assets/voucher/'.$op->voucher3)}}" alt="">
			</th>
		</tr>
		<tr>
			<th colspan="2">
				<img src="{{url('assets/voucher/'.$op->voucher4)}}" alt="">
			</th>
		</tr>
		<tr>
		  <th colspan="2"><tr>
		  <th colspan="2"><img src="{{asset('assets/images/operacion-pie.png')}}"></th>
	  </tr></th>
	  </tr>
	</table>
</div>