@extends('layouts.app')

@push('css')
    <!-- select2 Plugin -->
    <link href="{{asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet">	
            
    <!--mutipleselect css-->
    <link href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet">
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    Operación
@endpush

@push('titulo')
    Operación
@endpush


@section('content')
    @php
        $ban=\App\Modelo\Cuentabancaria::with("tipo","moneda","banco")->where("usuario_id",\Auth::User()->usuario_id)->get();
        $mos=\App\Modelo\Moneda::all();
        $cud=\App\Modelo\Cuentabancaria::with("tipo","moneda","banco")->where("usuario_id",1)->get();
    @endphp

    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="card">
                <div class="card-body" id ="card-body-x">
                    <div class="row">
                        <div class="col-xl-9 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        <img src="{{asset('assets/images/boperacion.png')}}">
                                    </div>
                                </div>

                                <div class="card-body">
                                    <form id="form" method="POST" action="{{route('soperacion')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="list-group">

                                            {{-- 1era Solapa: Calcula --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title></h5>
												<img src="{{asset('assets/images/calcula.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="card-body overflow-hidden">
                                                            <div class="form-group ">
                                                                <label class="form-label">¿Desde qué cuenta nos envías tu dinero?</label>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" required name="baa">
                                                                    @foreach($ban as $ba)
                                                                        <option value="{{$ba->cuentabancaria_id}}">{{$ba->banco->nombre}} | {{$ba->nrocuenta}} @if(isset($ba->nrocuentacci)) - CCI | {{$ba->nrocuentacci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group ">
                                                                <label class="form-label">¿A qué cuenta deseas que depositemos el dinero?</label>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="cuenta" required>
                                                                    @foreach($ban as $ba)
                                                                        <option value="{{$ba->cuentabancaria_id}}">{{$ba->banco->nombre}} | {{$ba->nrocuenta}} @if(isset($ba->nrocuentacci)) - CCI | {{$ba->nrocuentacci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="form-group ">
                                                                <div class="form-label">
                                                                    Tu nos envías
                                                                </div>

                                                                <div class="custom-controls-stacked">
                                                                    @foreach($mos as $mo)
                                                                        <label class="custom-control custom-radio">
                                                                            <input type="radio" class="custom-control-input" name="moe" onchange="Calcular()" value="{{$mo->moneda_id}}" required>
                                                                            <span class="custom-control-label">{{$mo->nombre}}</span>
                                                                        </label>
                                                                    @endforeach
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="form-label">INGRESA EL MONTO</label>
                                                                <input type="text" class="form-control" id="monto" name="monto" placeholder="Ingrese el monto que nos envías" onkeyup="Calcular();" value="0.00">
                                                            </div>

                                                            <button type="button" class="btn btn-primary btn-block" onclick="Calcular()">CALCULAR</button>
                                                            
                                                            <div id="alerta" class="label label-success" style="visibility: hidden;">
                                                                Si vas a transferir más de U$5 000 (cinco mil dólares americanos), por favor comunícate con nosotros por Whatsapp <a href="https://wa.me/51946091321">AQUÍ</a>. Tenemos un cambio preferencial para ti. 
                                                            </div>

                                                            <br>
                                                            <br>

                                                            <div class="col-md-12 col-xl-12">
                                                                <div class="card">
                                                                    <div class="card-body">
                                                                        <div class="dash-widget text-center">
                                                                            <p>TÚ RECIBES</p>
                                                                            <h3>
                                                                                <span id="reci">
                                                                                </span>

                                                                                <span id="dine">
                                                                                </span>
																			</h3>
                                                                            <p class="mb-0 text-muted">
                                                                                <i class="fas fa-calculator fa-lg bg-warning"></i>
                                                                                Tipo de cambio usado
                                                                                <br>
                                                                                <center><span class="h4 font-weight-bold mb-0" id="tcus"></span></center>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- 2da Solapa: Transfiere --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title> </h5>
												<img src="{{asset('assets/images/transfiere.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="mt-3">
                                                            <div class="form-group">
                                                                <label class="form-label">Elije el banco a donde nos vas a transferir</label>
                                                                {{-- //TODO: esto estaba en el otro operacion.blade.php --}}
                                                                <input type="text" name="mor" id="mor" hidden>
                                                                <select class="form-control select2 custom-select" data-placeholder="Elija uno" name="bar" id="bar" style="width: 100% !important;">
                                                                    @foreach($cud as $ba)
                                                                        <option value="{{$ba->cuentabancaria_id}}">{{$ba->banco->nombre}} | {{$ba->nrocuenta}} @if(isset($ba->nrocuentacci)) - CCI | {{$ba->nrocuentacci}} @endif</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <ul class="list-group">
                                                                <li class="list-group-item">

                                                                </li>
                                                                <li class="list-group-item">
                                                                    TRANSFIÉRENOS
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="btra">
                                                                        </span>

                                                                        <span id="bmon">
                                                                        </span>
                                                                    </span>
                                                                    <br>
                                                                    A NUESTRA CUENTA
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="tcse">
                                                                        </span>

                                                                        <span id="ncse">
                                                                        </span>
                                                                    </span>
                                                                </li>
                                                                <li class="list-group-item">
                                                                    Y TÚ RECIBIRÁS
                                                                    <span class="badgetext h4 font-weight-bold mb-0">
                                                                        <span id="atra">
                                                                        </span>

                                                                        <span id="amon">
                                                                        </span>
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                                        
                                            {{-- 3era Solapa: Finalizar --}}
                                            <div class="list-group-item py-3" data-acc-step>
                                                <h5 class="mb-0" data-acc-title> </h5>
												<img src="{{asset('assets/images/finalizar.png')}}">
                                                <div data-acc-content>
                                                    <div class="my-3">
                                                        <div class="float-right">
                                                            <a id="mas_vouchers" style="cursor: pointer;">
                                                                Más vouchers&nbsp; <i class="fas fa-plus-circle"></i>
                                                            </a>
                                                        </div>
                                                        <br>
                                                        <div class="form-group">
                                                            <div class="form-label">Adjunta el voucher o captura de la transferencia</div>
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input vouchers" id="vou" name="vou" accept="image/png, .jpeg, .jpg, image/gif">
                                                                <label class="custom-file-label">Elegir archivo</label>
                                                            </div>
                                                        </div>
                                                        <div class="d-none" id="div_vou_2">
                                                            <div class="form-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input vouchers" id="vou2" name="vou2" accept="image/png, .jpeg, .jpg, image/gif">
                                                                    <label class="custom-file-label">Elegir archivo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-none" id="div_vou_3">
                                                            <div class="form-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input vouchers" id="vou3" name="vou3" accept="image/png, .jpeg, .jpg, image/gif">
                                                                    <label class="custom-file-label">Elegir archivo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="d-none" id="div_vou_4">
                                                            <div class="form-group">
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input vouchers" id="vou4" name="vou4" accept="image/png, .jpeg, .jpg, image/gif">
                                                                    <label class="custom-file-label">Elegir archivo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <label class="form-label">	

                                                        <img src="{{asset('assets/images/gracias.png')}}">
                                                         
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="text" id="cambio" name="cambio" hidden="true">
                                        <input type="text" id="compra" name="compra" hidden="true">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-md-12 layout-spacing">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title">
                                        TIPO DE CAMBIO @if(\Auth::User()->regdate==1) (preferencial) @endif
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-success text-center">
                                            <div class="card-body">
                                                <p class="mb-0 text-white-50">COMPRA</p>
                                                <i class="fas fa-arrow-alt-circle-up text-green mr-1"></i><h2 class=" mb-0 pure">0.00</h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-info text-centerblanco">
                                            <div class="card-body">
                                                <p class="mb-0 text-whiteblanco-50">VENTA</p>
                                                <i class="fas fa-arrow-alt-circle-down text-red mr-1"></i><h2 class=" mb-0 sale">0.00</h2>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-xl-12 col-lg-3">
                                        <div class="card bg-warning text-center">
                                            <div class="card-body">
                                                <p class="mb-0 text-white-50">El tipo de cambio variará en:</p><br>
                                                <i class="far fa-clock fa-lg"></i><br> 
                                                <h3 class=" mb-0 cronometro">0.00</h3>
                                                <h5 class=" mb-0">Minutos</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!--Accordion-Wizard-Form js-->
		<script src="{{asset('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
		<script src="{{asset('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
		<script src="{{asset('assets/js/wizard.js')}}"></script>
		
	<!--Select2 js -->
		<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

		<!--MutipleSelect js-->
		<script src="{{asset('assets/plugins/multipleselect/multiple-select.js')}}"></script>
		<script src="{{asset('assets/plugins/multipleselect/multi-select.js')}}"></script>

		<!-- Inline js -->
        <script src="{{asset('assets/js/select2.js')}}"></script>

        {{-- TODO: esta se usara solamente para notificar que caduco la sesion y eso? --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>

        @if(\Auth::User()->regdate==1)
            @php
                $dc = \Auth::User()->timestamp;
                $dv = \Auth::User()->previous_visit;
            @endphp
            <script>
                var dc = eval({{$dc}});
                var dv= eval({{$dv}});
                var cam=0.00;
                var cron=900;
                var pref=true;
                $(".pure").html(dc);
                $(".sale").html(dv);
                //timestamp dc
                //previous_visit dv
                //regdate 1,0
            </script>
        @else
            <script>
                var dc=0.00;
                var dv=0.00;
                var cam=0.00;
                var cron=900;
                var pref=false;
                $(document).ready(function(){
                    $.get("ftipocambio",function (msg) {
                        dc=eval(msg.compra);
                        dv=eval(msg.venta);
                        $(".pure").html(dc);
                        $(".sale").html(dv);
                    });
                    function tipocambio() {
                        if(cron===0){
                            $.get("ftipocambio",function (msg) {
                                dc=eval(msg.compra);
                                dv=eval(msg.venta);
                                $(".pure").html(dc);
                                $(".sale").html(dv);                    
                                let img = "{{asset('assets/images/tiempo.jpg')}}";
                                alertify.alert('', '<img src="'+ img +'">', function(){
                                    alertify.success('Ok');
                                    location.reload();
                                });
                                clearInterval(intervalo);
                            });
                        }else{
                            cron-=1;
                            var tim=parseInt(cron/60)+":"+(cron%60);
                            $('.cronometro').html(tim);
                        }
                    }
                    var intervalo = setInterval(tipocambio, 1000);
                });
            </script>
        @endif

        <script>
            $('.vouchers').on('change',function(e){
                //get the file name
                var fileName = e.target.files[0].name;
                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fileName);
            })
        </script>

        <script>
            var cant_vouchers = 1;
            $('#mas_vouchers').on('click',function(e){

                if(cant_vouchers < 4){
                    cant_vouchers += 1;

                    $('#div_vou_'+cant_vouchers).removeClass('d-none');
                }
                else{
                    alertify.set('notifier','position', 'top-right');
                    alertify.error("No se permite subir mas de 4 vouchers");
                }
            })
        </script>

        <script>
            function numberFormat(numero){
                // Variable que contendra el resultado final
                var resultado = "";
        
                // Si el numero empieza por el valor "-" (numero negativo)
                if(numero[0]=="-")
                {
                    // Cogemos el numero eliminando los posibles puntos que tenga, y sin
                    // el signo negativo
                    nuevoNumero=numero.replace(/\,/g,'').substring(1);
                }else{
                    // Cogemos el numero eliminando los posibles puntos que tenga
                    nuevoNumero=numero.replace(/\,/g,'');
                }
        
                // Si tiene decimales, se los quitamos al numero
                if(numero.indexOf(".")>=0)
                    nuevoNumero=nuevoNumero.substring(0,nuevoNumero.indexOf("."));
        
                // Ponemos un punto cada 3 caracteres
                for (var j, i = nuevoNumero.length - 1, j = 0; i >= 0; i--, j++)
                    resultado = nuevoNumero.charAt(i) + ((j > 0) && (j % 3 == 0)? ",": "") + resultado;
        
                // Si tiene decimales, se lo añadimos al numero una vez forateado con 
                // los separadores de miles
                if(numero.indexOf(".")>=0)
                    resultado+=numero.substring(numero.indexOf("."));
        
                if(numero[0]=="-")
                {
                    // Devolvemos el valor añadiendo al inicio el signo negativo
                    return "-"+resultado;
                }else{
                    return resultado;
                }
            }
        </script>
        
        <script type="text/javascript">
            $( document ).ready(function() {
                //Si es una pantalla chiquita, pero no de celular, pongo para que el menu no se alinee al centro y le saco un poco de padding.
                if(window.matchMedia("(min-width:100px) and (max-width: 500px)").matches){
                    $('#side-app').removeClass("side-app");
                    $("#card-body-x").removeClass("card-body");
                }
                else{
                    if(!$('#side-app').hasClass("side-app")){
                        $('#side-app').addClass("side-app");
                    }
                    if(!$('#card-body-x').hasClass("card-body")){
                        $("#card-body-x").removeClass("card-body");
                    }
                }
            });
        </script>
        
        <script>
            function Calcular(){
                $('#monto').val( numberFormat($("#monto").val()) );

                var m_aux = $('#monto').val();
                var m=parseFloat($('#monto').val().replace(/,/, ''));
                var d=$('input[name=moe]:checked').val();
                if(d==2){
                    if(!pref && eval(m)>=eval(5000)){
                        $("#alerta").css("visibility","visible");
                    }
                    $("#bar option").remove();
                    bd.forEach(cuenta=>$("#bar").append('<option value="'+cuenta.cuentabancaria_id+'">-'+cuenta.tipo.nombre+' | '+cuenta.moneda.nombre+' | '+cuenta.banco.nombre+'- '+cuenta.nrocuenta+'</option>'));
                    cam=(eval(m)*eval(dc)).toFixed(2);
                    $("#reci").html( "S/" );
                    $("#reci").append( numberFormat(cam) );
                    $("#dine").html("Soles");
                    $("#cambio").val(cam);
                    $("#compra").val(dc);
                    $("#tcus").html(dc);
                    $("#atra").html("S/");
                    $("#atra").append(cam);
                    $("#amon").html("Soles");
                    $("#btra").html("$");
                    $("#btra").append(m_aux);
                    $("#bmon").html("Dólares");
                    $("#mor").val(1);
                }else{
                    $("#bar option").remove();
                    bs.forEach(cuenta=>$("#bar").append('<option value="'+cuenta.cuentabancaria_id+'">-'+cuenta.tipo.nombre+' | '+cuenta.moneda.nombre+' | '+cuenta.banco.nombre+'- '+cuenta.nrocuenta+'</option>'));
                    cam=(eval(m)/eval(dv)).toFixed(2);
                    $("#reci").html("$");
                    $("#reci").append( numberFormat(cam) );
                    $("#dine").html("Dólares");
                    $("#cambio").val(cam);
                    $("#tcus").html(dv);
                    $("#atra").html("$");
                    $("#atra").append(cam);
                    $("#amon").html("Dólares");
                    $("#btra").html("S/");
                    $("#btra").append(m_aux);
                    $("#bmon").html("Soles");
                    $("#compra").val(dv);
                    $("#mor").val(2);
                }
            }
            var banco=<?php echo $cud ?>;
            var bs=banco.filter(cuenta => cuenta.moneda.moneda_id==1);
            var bd=banco.filter(cuenta => cuenta.moneda.moneda_id==2);
        
            $(document).ready(function(){
                $("input[name=moe][value='1']").prop("checked",true);
                function selecte() {
                    var select=$("#bar").val();
                    var objeto = banco.find(function(element) {
                        if(element.cuentabancaria_id==select){
                            return element;
                        }else{
                            return false;
                        }
                    });
                    $("#tcse").html(objeto.banco.nombre+"- "+objeto.tipo.nombre+" "+objeto.moneda.nombre);
                    $("#ncse").html(objeto.nrocuenta); 
                }
                $("#bar").change(function(){
                    selecte();
                });
                selecte();
            });
            function valida(){
                if($("#monto").val()==""){
                    alertify.error("Ingresa el monto");
                }
                if($("#vou").val()==""){
                    alertify.error("Sube una captura del comprobante");
                }
            }
            $(document).on('submit', '#form', function (e) {
                $('#monto').val( parseFloat($('#monto').val().replace(/,/, '')) );

                var input = ($("#vou"))[0];
                var file = input.files[0];
                var zz=eval(cam)<1;
                var yy=eval(file.size>8000000);
                if(zz){
                    alertify.error("Aplique la taza de Cambio");
                    e.preventDefault();
                } 
                if(yy){
                    $("#imagen").css("visibility","visible");
                    alertify.error("Error al cargar archivo");
                    e.preventDefault();
                }
                if(!zz&&!yy){
                    $("#global-loader").show();
                }
            });
        </script>
@endsection