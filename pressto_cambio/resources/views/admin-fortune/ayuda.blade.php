@extends('layouts.app')

@push('css')
    <!-- select2 Plugin -->
    <link href="{{asset('assets/plugins/select2/select2.min.css')}}" rel="stylesheet" />
    <!--mutipleselect css-->
    <link href="{{asset('assets/plugins/multipleselect/multiple-select.css')}}" rel="stylesheet">
@endpush

@push('titulo_completo')
    ¿Necesitas ayuda?
@endpush

@push('titulo')
    Ayuda
@endpush

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                      <img src="https://www.dls.com.pe/app/assets/images/ayuda.png">
                </div>

                <div class="card-body">
                    <div id="accordion" class="w-100 ">
                        <div class="card mb-0 border">
                            <div class="accor bg-primary" id="headingOne">
                                <h5 class="m-0">
                                    <a href="#collapseOne" class="text-white" data-toggle="collapse" aria-expanded="true" aria-controls="collapseOne">
                                    ¿Qué tan seguro es usar DLS Perú para cambiar dinero online?
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    Somos una empresa constituida formalmente y con la regulación respectiva de la Superintendencia de Banca, Seguros y AFP - SBS y además somos socios de la Cámara de Comercio de Lima - CCL
                                </div>
                            </div>
                        </div>

                        <div class="card mb-0 border">
                            <div class="accor  bg-primary" id="headingTwo">
                                <h5 class="m-0">
                                    <a href="#collapseTwo" class="collapsed text-white" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                                      ¿Cómo inicio una transferencia para cambiar dólares?
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse b-b0" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                   Puedes contactarnos al + (511) 304-2827 / 938.123380 / 947.222833 o al correo electrónico operaciones@dls.com.pe o info@dls.com.pe
                                </div>
                            </div>
                        </div>

                        <div class="card border mb-0">
                            <div class="accor  bg-primary" id="headingThree">
                                <h5 class="m-0">
                                    <a href="#collapseThree" class="collapsed text-white" data-toggle="collapse" aria-expanded="false" aria-controls="collapseThree">
                                       ¿Debo pagar por usar la plataforma?
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse b-b0" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    Nosotros no cobramos comisión por el servicio
                                </div>
                            </div>
                        </div>																								<div class="card border mb-0">                            <div class="accor  bg-primary" id="headingFour">                                <h5 class="m-0">                                    <a href="#collapseFour" class="collapsed text-white" data-toggle="collapse" aria-expanded="false" aria-controls="collapseFour">                                      ¿Cómo realizan el pago?                                    </a>                                </h5>                            </div>                            <div id="collapseFour" class="collapse b-b0" aria-labelledby="headingFour" data-parent="#accordion">                                <div class="card-body">                                   El pago se realiza a través de transferencias por internet desde una cuenta bancaria. Según las normas de la Superintendencia de Banca, Seguros y AFPs – SBS, solo se aceptarán operaciones en línea.                                </div>                            </div>                        </div>						
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

@endsection