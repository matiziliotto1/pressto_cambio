<div>
	<table class="table">
		<tr>
		  <th colspan="2"><img src="{{asset('assets/images/operacionheader.png')}}"></th>
	    </tr>
		<tr>
			<th>Fecha de Operación</th>
			<td>{{$operacion->created_at}}</td>
		</tr>
		<tr>
			<th>Fecha de Actualización</th>
			<td>{{$operacion->updated_at}} {{$operacion->ultimo_usuario}}</td>
		</tr>
		@if(isset($operacion->usuario_personal))
			<tr>
				<th>Nombres</th>
				<td>{{$operacion->usuario_personal->primer_nombre}} {{$operacion->usuario_personal->segundo_nombre}}</td>
			</tr>
			<tr>
				<th>Apellidos</th>
				<td>{{$operacion->usuario_personal->primer_apellido}} {{$operacion->usuario_personal->segundo_apellido}}</td>								
			</tr>
		@elseif(isset($operacion->usuario_empresa))
			<tr>
				<th>Nombres</th>
				<td>{{$operacion->usuario_empresa->primer_nombre_rp}} {{$operacion->usuario_empresa->segundo_nombre_rp}}</td>
			</tr>
			<tr>
				<th>Apellidos</th>
				<td>{{$operacion->usuario_empresa->primer_apellido_rp}} {{$operacion->usuario_empresa->segundoa_pellido_rp}}</td>								
			</tr>
		@endif
		<tr>
			<th>Cuenta de Envio</th>
			<td>-{{$operacion->cuenta_bancaria_envio[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_envio[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_envio->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_envio->numero_cuenta_cci}} @endif</td>
		</tr>
		<tr>
			<th>Monto Enviado</th>
			<td><strong>@if($operacion->tipo_moneda_envia->id_moneda == 1) S/ @else $ @endif {{number_format($operacion->monto, 2, '.', ',')}}</strong> @if($operacion->tipo_moneda_envia->id_moneda == 1) Soles @else Dólares @endif</td>
		</tr>
		<tr>
			<th>Cuenta Destino</th>
			<td>-{{$operacion->cuenta_bancaria_deposita[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_deposita[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_deposita->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_deposita->numero_cuenta_cci}} @endif</td>
		</tr>
		<tr>
			<th>Monto Esperado</th>
			<td><strong>@if($operacion->tipo_moneda_recibe->id_moneda == 1) S/ @else $ @endif {{number_format($operacion->cambio, 2, '.', ',')}}</strong> @if($operacion->tipo_moneda_recibe->id_moneda == 1) Soles @else Dólares @endif</td>
		</tr>
		<tr>
			<th>Cuenta de Depósito</th>
			<td>-{{$operacion->cuenta_bancaria_transfiere[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_transfiere[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_transfiere->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_transfiere->numero_cuenta_cci}} @endif</td>
		</tr>
		@foreach ($operacion->vouchers as $voucher)
			<tr>
					<th>Voucher adjunto N° {{$loop->index+1}}</th>
					<td>
						<img src="{{url('assets/voucher/'.$voucher->voucher)}}" alt="">
					</td>
			</tr>
		@endforeach
	</table>
</div>