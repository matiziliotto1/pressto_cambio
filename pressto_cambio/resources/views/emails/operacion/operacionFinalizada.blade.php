<html>
	<head>
		<meta content="text/html; charset=utf-8" http-equiv=Content-Type>
		<meta name=GENERATOR content="MSHTML 11.00.10570.1001">
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
	
		<style>
	
		</style>
	</head>
<body>

<div class="container-fluid">
	<center>
		<table class="voucher">
			<tr>
				<th colspan="2"><a href="#"><img src="http://dls.com.pe/app/img_mail/cabecera.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/cabecera.jpg" alt="DLS"></a></td>
            </tr>
            
            <tr class="headt">
				<th colspan="2">
					<span class="text-blue">
                        <h1>¡FELICITACIONES!<BR>{{$mensaje}}</h1></span>
                    </span>
				</th>
			</tr>
			<tr>
				<th colspan="2"><img src="{{asset('assets/images/operacionheader.png')}}"></th>
			</tr>
			<tr>
				<th align="right">Fecha de Operación</th>
				<td>{{$operacion->created_at}}</td>
			</tr>
			<tr>
				<th align="right">Fecha de Actualización</th>
				<td>{{$operacion->updated_at}} {{$operacion->ultimo_usuario}}</td>
            </tr>
            @if(isset($operacion->usuario_personal))
                <tr>
                    <th>Nombres</th>
                    <td>{{$operacion->usuario_personal->primer_nombre}} {{$operacion->usuario_personal->segundo_nombre}}</td>
                </tr>
                <tr>
                    <th>Apellidos</th>
                    <td>{{$operacion->usuario_personal->primer_apellido}} {{$operacion->usuario_personal->segundo_apellido}}</td>								
                </tr>
            @elseif(isset($operacion->usuario_empresa))
                <tr>
                    <th>Nombres</th>
                    <td>{{$operacion->usuario_empresa->primer_nombre_rp}} {{$operacion->usuario_empresa->segundo_nombre_rp}}</td>
                </tr>
                <tr>
                    <th>Apellidos</th>
                    <td>{{$operacion->usuario_empresa->primer_apellido_rp}} {{$operacion->usuario_empresa->segundoa_pellido_rp}}</td>								
                </tr>
            @endif

			@if(isset($email_cliente))
				<tr>
					<th align="right">Email</th>
					<td>{{$email_cliente}}</td>
				</tr>
			@endif
			@if(isset($dni))
				<tr>
					<th align="right">DNI</th>
					<td>{{$dni}}</td>
				</tr>
            @endif
            
            <tr>
                <th>Cuenta de Envio</th>
                <td>-{{$operacion->cuenta_bancaria_envio[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_envio[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_envio->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_envio->numero_cuenta_cci}} @endif</td>
            </tr>
            <tr>
                <th>Monto Enviado</th>
                <td><strong>@if($operacion->tipo_moneda_envia->id_moneda == 1) S/ @else $ @endif {{number_format($operacion->monto, 2, '.', ',')}}</strong> @if($operacion->tipo_moneda_envia->id_moneda == 1) Soles @else Dólares @endif</td>
            </tr>
            <tr>
                <th>Cuenta Destino</th>
                <td>-{{$operacion->cuenta_bancaria_deposita[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_deposita[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_deposita->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_deposita->numero_cuenta_cci}} @endif</td>
            </tr>
            <tr class="sep">
				<th>Tipo de Cambio</th>
				<td><strong>{{$operacion->taza}}</strong></td>
			</tr>
            <tr>
                <th>Monto Esperado</th>
                <td><strong>@if($operacion->tipo_moneda_recibe->id_moneda == 1) S/ @else $ @endif {{number_format($operacion->cambio, 2, '.', ',')}}</strong> @if($operacion->tipo_moneda_recibe->id_moneda == 1) Soles @else Dólares @endif</td>
            </tr>
            <tr>
                <th>Cuenta de Depósito</th>
                <td>-{{$operacion->cuenta_bancaria_transfiere[0]['banco']->nombre}} - {{$operacion->cuenta_bancaria_transfiere[0]['numero_cuenta']}} @if(isset($operacion->cuenta_bancaria_transfiere->numero_cuenta_cci)) | {{$operacion->cuenta_bancaria_transfiere->numero_cuenta_cci}} @endif</td>
            </tr>
            @foreach ($operacion->vouchers as $voucher)
                <tr>
                        <th>Voucher adjunto N° {{$loop->index+1}}</th>
                        <td>
                            <img src="{{url('assets/voucher/'.$voucher->voucher)}}" alt="">
                        </td>
                </tr>
            @endforeach

			<tr>
				<th colspan="2"><img src="{{asset('assets/images/operacion-pie.png')}}"></th>
			</tr>

			<tr>
				<th colspan="2"><a href="#"><img src="http://dls.com.pe/app/img_mail/dlspie.jpg" class="sticky-logo" data-retina="http://dls.com.pe/app/img_mail/dlspie.jpg" alt="DLS"></a></td>
			</tr>
			
		</table>
	</center>
</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
