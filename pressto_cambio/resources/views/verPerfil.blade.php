@extends('layouts.app')

@push('titulo_completo')
	Perfil de usuario
@endpush

@push('titulo')
Editar Perfil
@endpush

@push('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css">
	<style>
		.ajs-ok{
			border: none;
			background-color: green;
			color: white;
			border-radius: 5px;
		}
		.ajs-cancel{
			border: none;
			background-color: red;
			color: white;
			border-radius: 5px;
		}
		.edita{
			color: green;
		}
		.ajs-message{color: white;}
	</style>
@endpush

@section('content')
	<div class="row">
		<div class="col-xl-12  col-md-12">
            <form @if(\Auth::user()->tipo_perfil_activo == 0) action="{{route('updatePerfilPersonal', \Auth::user()->perfil_activo)}}" @elseif(\Auth::user()->tipo_perfil_activo == 1) action="{{route('updatePerfilEmpresa', \Auth::user()->perfil_activo)}}" @endif id="fus" method="POST">
				@csrf
				<div class="card">
					<div class="card-header">
						<div class="card-title">Editar Datos de Perfil</div>
					</div>
					<div class="card-body">
						<div class="card-title font-weight-bold"><i class="mdi mdi-account-card-details" data-toggle="tooltip" title="mdi-account-card-details"></i> Datos</div>
						<div class="row">
							@if(\Auth::User()->tipo_perfil_activo == 1)
								<div class="col-sm-6 col-md-6">
									<div class="form-group ">
										<label class="form-label">Registro Único de Contribuyentes (RUC)</label>
										<input type="text" class="form-control" placeholder="Número de ruc" required  name="ruc" value="{{$perfil_usuario->ruc}}" readonly>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group ">
										<label class="form-label edita">Razón social</label>
										<input type="text" class="form-control" placeholder="Razón social" required  name="razon_social" value="{{$perfil_usuario->razon_social}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group ">
										<label class="form-label edita">Giro de negocio</label>
										<input type="text" class="form-control" placeholder="Giro de negocio" required  name="giro_negocio" value="{{$perfil_usuario->giro_negocio}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Dirección fiscal</label>
									<input type="text" class="form-control" placeholder="City" name="direccion_fiscal" required value="{{$perfil_usuario->direccion_fiscal}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">País</label>
									<select class="form-control custom-select" id="pad" name="id_pais" required>
										@foreach($paises as $pais)
											<option value="{{$pais->id_pais}}">{{$pais->nombre}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Departamento</label>
									<select name="id_departamento" class="form-control custom-select" id="dep" required></select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Provincia</label>
									<select name="id_provincia" class="form-control custom-select" id="pro" required></select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Distrito</label>
									<select name="id_distrito" class="form-control custom-select" id="dis" required></select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Correo electrónico</label>
									<input type="text" class="form-control" name="email" maxlength="35"  placeholder="Correo electrónico" value="{{$perfil_usuario->email}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Teléfono</label>
									<input type="text" class="form-control" name="telefono" maxlength="50"  placeholder="Teléfono" value="{{$perfil_usuario->telefono}}">
								</div>


								<div class="col-sm-6 col-md-6">
									<div class="card-title font-weight-bold">Legal</div>
									<div class="form-group">
										<label class="form-label edita">Primer Nombre</label>
										<input type="text"  class="form-control" placeholder="First Name" required name="primer_nombre_rp" value="{{$perfil_usuario->primer_nombre_rp}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
								<div class="card-title font-weight-bold">&nbsp;</div>
									<div class="form-group">
										<label class="form-label edita">Segundo Nombre</label>
										<input type="text" class="form-control" placeholder="Second Name" name="segundo_nombre_rp" value="{{$perfil_usuario->segundo_nombre_rp}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Apellido Paterno</label>
										<input type="text" class="form-control" placeholder="Last Name" name="primer_apellido_rp" value="{{$perfil_usuario->primer_apellido_rp}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Apellido Materno</label>
										<input type="text" class="form-control" placeholder="Last Name" name="segundo_apellido_rp" value="{{$perfil_usuario->segundo_apellido_rp}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group ">
										<label class="form-label edita">Tipo de Documento</label>
										<select class="form-control select2 custom-select" id="td_rp" name="id_tipo_documento_rp" >
											@foreach($tipos_documentos as $tipo_documento)
                                                <option value="{{$tipo_documento->id_tipo_documento}}">{{$tipo_documento->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Número de Documento</label>
										<input type="text" class="form-control" placeholder="Numero de Documento" required  name="numero_documento_rp" value="{{$perfil_usuario->numero_documento_rp}}">
									</div>
								</div>


								<div class="col-sm-6 col-md-6">
									<div class="card-title font-weight-bold">Contacto</div>
									<div class="form-group">
										<label class="form-label edita">Primer nombre (contacto)</label>
										<input type="text"  class="form-control" placeholder="First Name" required name="primer_nombre_c" value="{{$perfil_usuario->primer_nombre_c}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
								<div class="card-title font-weight-bold">&nbsp;</div>
									<div class="form-group">
										<label class="form-label edita">Segundo nombre (contacto)</label>
										<input type="text" class="form-control" placeholder="Second Name" name="segundo_nombre_c" value="{{$perfil_usuario->segundo_nombre_c}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Apellido paterno (contacto)</label>
										<input type="text" class="form-control" placeholder="Last Name" name="primer_apellido_c" value="{{$perfil_usuario->primer_apellido_c}}">
									</div>
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Apellido materno (contacto)</label>
										<input type="text" class="form-control" placeholder="Last Name" name="segundo_apellido_c" value="{{$perfil_usuario->segundo_apellido_c}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Tipo de Documento (Contacto)</label>
									<select class="form-control select2 custom-select" id="td_c" name="id_tipo_documento_c" >
										@foreach($tipos_documentos as $tipo_documento)
                                            <option value="{{$tipo_documento->id_tipo_documento}}">{{$tipo_documento->nombre}}</option>
                                        @endforeach
									</select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Número de documento (contacto)</label>
									<input type="text" class="form-control" name="numero_documento_c" maxlength="35"  placeholder="Número de documento" value="{{$perfil_usuario->numero_documento_c}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Teléfono de contacto</label>
									<input type="text" class="form-control" name="telefono_c" maxlength="35"  placeholder="Teléfono de contacto" value="{{$perfil_usuario->telefono_c}}">
                                </div>

							@elseif(\Auth::User()->tipo_perfil_activo == 0)
								<div class="form-group col-md-6">
									<label class="form-label edita">Primer Nombre</label>
									<input type="text"  class="form-control" placeholder="First Name" required name="primer_nombre" value="{{$perfil_usuario->primer_nombre}}">
								</div>
								<div class="col-sm-6 col-md-6">
									<div class="form-group">
										<label class="form-label edita">Segundo Nombre</label>
										<input type="text" class="form-control" placeholder="Second Name" name="segundo_nombre" value="{{$perfil_usuario->segundo_nombre}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Apellido Paterno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="primer_apellido" value="{{$perfil_usuario->primer_apellido}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Apellido Materno</label>
									<input type="text" class="form-control" placeholder="Last Name" name="segundo_apellido" value="{{$perfil_usuario->segundo_apellido}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Tipo de Documento</label>
									<select class="form-control select2 custom-select" id="id_tipo_documento" name="id_tipo_documento">
										@foreach($tipos_documentos as $tipo_documento)
                                            <option value="{{$tipo_documento->id_tipo_documento}}">{{$tipo_documento->nombre}}</option>
                                        @endforeach
									</select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Número de Documento</label>
									<input type="text" class="form-control" placeholder="Numero de Documento" required  name="numero_documento" value="{{$perfil_usuario->numero_documento}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Fecha de Nacimiento</label>
									<input type="date" class="form-control" value="{{$perfil_usuario->fecha_nacimiento}}" name="fecha_nacimiento">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Teléfono</label>
									<input type="text" class="form-control" name="telefono" maxlength="30"  placeholder="Teléfonos de contacto" value="{{$perfil_usuario->telefono}}">
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Celular</label>
									<input type="text" class="form-control" name="celular1" maxlength="35" value="{{$perfil_usuario->celular1}}"  placeholder="Número de Celular" required>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Celular 2</label>
									<input type="text" class="form-control" name="celular2" maxlength="50" value="{{$perfil_usuario->celular2}}"  placeholder="Otro Número">
								</div>

								<div class="form-group col-md-6">
									<div class="card-title font-weight-bold">Domicilio</div>
									<label class="form-label edita">País</label>
									<select class="form-control custom-select" id="pad" name="id_pais" required>
										@foreach($paises as $pais)
											<option value="{{$pais->id_pais}}">{{$pais->nombre}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-6">
									<div class="card-title font-weight-bold">&nbsp;</div>
									<label class="form-label edita">Departamento</label>
									<select name="id_departamento" class="form-control custom-select" id="dep" required></select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Provincia</label>
									<select name="id_provincia" class="form-control custom-select" id="pro" required></select>
								</div>
								<div class="form-group col-md-6">
									<label class="form-label edita">Distrito</label>
									<select name="id_distrito" class="form-control custom-select" id="dis" required></select>
								</div>
								<div class="form-group col-md-12">
									<label class="form-label edita">Dirección</label>
									<input type="text" class="form-control" placeholder="City" name="direccion" required value="{{$perfil_usuario->direccion}}">
								</div>
								

								<div class="form-group col-md-6">
									<div class="card-title font-weight-bold">Ocupación</div>
									<label class="form-label edita">Ocupación</label>
									<select class="form-control select2 custom-select" id="oc" name="id_ocupacion">
										@foreach($ocupaciones as $ocupacion)
											<option value="{{$ocupacion->id_ocupacion}}">{{$ocupacion->nombre}}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-6">
									{{-- --}}
								</div>
								<div class="form-group col-md-6">
									<div class="form-label edita">¿Es Usted una persona expuesta políticamente?</div>
									<div class="custom-controls-stacked">
										<label class="custom-control custom-radio">
											<input required type="radio" class="custom-control-input required" name="persona_expuesta" value="1" {{$perfil_usuario->persona_expuesta ? "checked":""}}>
											<span class="custom-control-label">Si</span>
										</label>
										<label class="custom-control custom-radio">
											<input required type="radio" class="custom-control-input required" name="persona_expuesta" value="0" {{$perfil_usuario->persona_expuesta ? "":"checked"}}>
											<span class="custom-control-label">No</span>
										</label>
									</div>
								</div>
							@endif
						</div>
					</div>
					<div class="card-footer text-right">
						<button href="#" class="btn btn-primary">Cambiar</button>
						<a href="#" class="btn btn-danger">Cancelar</a>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>

	<script>
		$(document).ready(function(){
            if({{\Auth::user()->tipo_perfil_activo}} == 0){
                $("#oc").val("{{$perfil_usuario->id_ocupacion}}");
                $("#td").val("{{$perfil_usuario->id_tipo_documento}}");
                $('#pad option[value="{{$perfil_usuario->id_pais}}"]').attr("selected", "selected");
            }
            else if({{\Auth::user()->tipo_perfil_activo}} == 1){
                $("#td_rp").val("{{$perfil_usuario->id_tipo_documento_rp}}");
                $("#td_c").val("{{$perfil_usuario->id_tipo_documento_c}}");
                $('#pad option[value="{{$perfil_usuario->id_pais}}"]').attr("selected", "selected");
            }

			$.get("{{url('api/departamento')}}").then(function(ls){
				for (var i =  1; i < ls.length; i++) {
					$("#dep").append('<option value="'+ls[i].dDepartamento+'">'+ls[i].Descripcion+'</option>');
				}
				$("#dep").val("{{$perfil_usuario->id_departamento}}");
				provincia($("#dep").val());
			});
			function provincia(i) {
				$("#pro option").remove();
				$.get("{{url('api/provincia')}}/"+i).then(function(ls){
					for (var i =  1; i < ls.length; i++) {
						$("#pro").append('<option value="'+ls[i].codProvincia+'">'+ls[i].Descripcion+'</option>');
					}
					$("#pro").val("{{$perfil_usuario->id_provincia}}");
					distrito($("#dep").val(),$("#pro").val());
				});
			}
			function distrito(i,j) {
				$("#dis option").remove();
				$.get("{{url('api/distrito')}}/"+i+"/"+j).then(function(ls){
					for (var i =  1; i < ls.length; i++) {
						$("#dis").append('<option value="'+ls[i].codDistrito+'">'+ls[i].Descripcion+'</option>');
					}
					$("#dis").val("{{$perfil_usuario->id_distrito}}");
				});
			}
			$('#dep').on('change', function(){
				provincia($("#dep").val());
			});
			$('#pro').on('change', function(){
				distrito($("#dep").val(),$("#pro").val());
			});                    
		});
	</script>
@endsection