@extends('layouts.app2')

@push('titulo_completo')
	Tipo de Cambio
@endpush

@push('titulo')
	Tipo de Cambio
@endpush

@push('css')
<link href="{{url('assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" />
<link href="{{url('assets/plugins/datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet">
<link href="{{url('assets/plugins/datatable/responsive.bootstrap4.min.css')}}" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css">
<style>
	.ajs-ok{
		border: none;
		background-color: green;
		color: white;
		border-radius: 5px;
	}
	.ajs-cancel{
		border: none;
		background-color: red;
		color: white;
		border-radius: 5px;
	}
</style>
@endpush

@section('content')
	<div class="row">
		<div class="col-sm-3">
			<div class="card text-white bg-primary ">
				<div class="card-header">
					<div class="card-title">Tipo de Cambio</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
                        <form id="saveTipoCambio">
							<table class="table" >
								<tr>
									<th>Compra:
									<input id="compra" required class="form-control" type="text" name="compra"></th>
								</tr>
								<tr>
									<th>Venta:
									<input id="venta" required class="form-control" type="text" name="venta"></th>
								</tr>
								<tr>								
									<th> 
										<center>
											<button class="btn btn-success" type="submit"><i class="fa fa-save fa-5x"></i> Guardar</button>
											
										</center>
									</th>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-9">
			<div class="card">
				<div class="card-header">
					<div class="card-title">Historial de tipo de cambio</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table id="example" class="table table-striped table-bordered" >
							<thead>
								<tr>
									<th>Fecha</th>
									<th>Compra</th>
									<th>Venta</th>
									<th>Usuario</th>								
								</tr>
							</thead>
							<tbody>
								@foreach($tipos_cambio as $tipo_cambio)
								<tr>
									<td>{{$tipo_cambio->created_at}}</td>
									<td>{{$tipo_cambio->compra}}</td>
									<td>{{$tipo_cambio->venta}}</td>
									<td>{{$tipo_cambio->creado_por}}</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{url('assets/plugins/input-mask/jquery.mask.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/jquery.dataTables.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/dataTables.bootstrap4.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/buttons.bootstrap4.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/jszip.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/pdfmake.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/vfs_fonts.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
	<script src="{{url('assets/plugins/datatable/responsive.bootstrap4.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
	<script>
		var tabla;
		$(document).ready(function() {
			tabla = $('#example').DataTable( {
				lengthChange: false,
				buttons: ['excel', 'pdf', 'colvis' ]
			});
			tabla.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
			tabla.column( '0:visible' ).order( 'desc' ).draw();
		});

		$(document).on('submit', '#saveTipoCambio', function (e) {
			e.preventDefault();

            let token = '{{csrf_token()}}';
            let compra = $("#compra").val();
            let venta = $("#venta").val();

            $.ajax({
                url: '{{route("saveTipoDeCambio")}}',
                type: 'post',
                data: {
                    compra: compra,
                    venta: venta,
                    _token: token
                },
                dataType: 'json',
                beforeSend: function(){
                },
                success: function(response){
                    if(response['success']){
						console.log(response['tipo_de_cambio']);
						let created_at = response['tipo_de_cambio'].created_at;

						const regex1 = /T/gi;
						const regex2 = /.000000Z/gi;
						created_at = created_at.replace(regex1, ' ');
						created_at = created_at.replace(regex2, '');

						let compra = response['tipo_de_cambio'].compra;
						let venta = response['tipo_de_cambio'].venta;
						let creado_por = response['tipo_de_cambio'].creado_por;

                        tabla.row.add([
                            created_at,
                            compra,
                            venta,
                            creado_por,
                        ]).draw(false);

                        clearFormTipoCambio();
                        alertify.success(response['message']);
                    }
                    else{
                        alertify.error(response['message']);
                    }
                },
                complete:function(data){
                }
            });
		});

		function clearFormTipoCambio() {
			$("#compra").val("");
			$("#venta").val("");
		}
	</script>
@endsection