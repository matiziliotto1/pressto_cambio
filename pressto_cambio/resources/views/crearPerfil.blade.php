@php
	$pas=\App\Pais::all();
	$doc=\App\Tipo_documento::all();
	$ocs=\App\Ocupacion::all();
@endphp 

<!DOCTYPE html>
<html lang="en">
<head>
	<title> Registro sistema DLS</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="msapplication-TileColor" content="#0061da">
	<meta name="theme-color" content="#1643a3">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">

	<link rel="icon" href="{{asset('assets/images/brand/favicon.ico')}}" type="image/x-icon">
	<link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/brand/favicon.ico')}}">

	<!-- description -->
	<meta name="description" content="Formulario de Registro">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{asset('assets/registro/css/bootstrap.min.css')}}">
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css">
	<!-- Fonts and icons -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700" rel="stylesheet"> 
	<!-- Reset CSS -->
	<link rel="stylesheet" href="{{asset('assets/registro/css/reset.css')}}">
	<!-- Style CSS -->
	<link rel="stylesheet" href="{{asset('assets/registro/css/style.css')}}">
	<!-- Responsive  CSS -->
    <link rel="stylesheet" href="{{asset('assets/registro/css/responsive.css')}}">
    
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/css/alertify.min.css')}}" rel="stylesheet">

</head>
<body>
	<div class="wizard-main">
		<div id="particles-js"></div>

		<div class="container">
			<div class="row">
				<div class="col-lg-6 banner-sec">
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner" role="listbox">
							<div class="carousel-item active">
								<img class="d-block img-fluid" src="{{asset('assets/registro/images/slider-01.jpg')}}" alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Nosotros</h2>
										<p>Somos una empresa constituida formalmente y con la regulación respectiva de la Superintendencia de Banca, Seguros y AFP – SBS, nuestro de registro es 00555-2020</p>
									</div>	
								</div>
							</div>

							<div class="carousel-item">
								<img class="d-block img-fluid" src="{{asset('assets/registro/images/slider-02.jpg')}}" alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Comunicate</h2>
										<p>Recuerda que también puedes enviarnos un Whastapp al <br>938-123380 indicándonos el monto de tu operación y los datos de la transferencia.</p>
									</div>	
								</div>
							</div>

							<div class="carousel-item">
								<img class="d-block img-fluid" src="{{asset('assets/registro/images/slider-03.jpg')}}" alt="First slide">
								<div class="carousel-caption d-none d-md-block">
									<div class="banner-text">
										<h2>Seguridad</h2>
										<p>En DLS Perú, nunca te pediremos contraseñas ni datos privados para tus transacciones, la SBS se encarga de supervisar la transparencia de las operaciones de nuestra plataforma.</p>
									</div>	
								</div>
							</div>
						</div>	   
					</div>
				</div>

				<div class="col-lg-6 login-sec">
					<div class="login-sec-bg">
						<h2 class="text-center">REGISTRO A NUESTRO SISTEMA</h2>
						<form id="example-advanced-form" action="" style="display: none;" method="POST">
							@csrf

							{{-- Solapa 1.Perfil --}}
							<h3>Perfil</h3><br>

							<fieldset class="form-input">
                                <div class="form-group col-md-6">
									<label class="form-label">Perfiles</label>
									<select class="form-control select2 custom-select" name="perfiles" id="perfiles" data-placeholder="Elija uno"></select>
                                </div>
                                
								<center>
									<h4>
										¿Con qué perfil deseas operar?
									</h4>

                                    <main class="figures">
                                        <article>  Selecciona esta opción si compras o vendes dólares <br>con fines personales. (DNI, CE o pasaporte)
                                            <figure data-description="PERSONA">
                                                <img id="persona" src="{{asset('assets/images/brand/persona.png')}}" class="thumb persona_o_empresa"><br><br>
                                            </figure>
                                        </article>

                                        <article> Selecciona esta opción si compras o vendes dólares <br>a nombre de una empresa (RUC).
                                            <figure data-description="EMPRESA">
                                                <img id="empresa" src="{{asset('assets/images/brand/empresa.png')}}" class="thumb persona_o_empresa">
                                            </figure>
                                        </article>
                                    </main>

									<input type="text" name="persona_o_empresa_selected" id="persona_o_empresa_selected" class="invisible" required>

									<br>
									<br>
								</center>
							</fieldset>

							{{-- Solapa 2.Datos --}}
							<h3>Datos</h3>

							<fieldset class="form-input">
								{{-- Aca se agregan con javascript las solapas enteras dependiendo si selecciono empresa o usuario --}}
							</fieldset>

							{{-- Solapa 3.Legal --}}
							<h3>Domicilio</h3>

							<fieldset class="form-input">
								{{-- Aca se agregan con javascript las solapas enteras dependiendo si selecciono empresa o usuario --}}
							</fieldset>

							{{-- Solapa 4.Contacto --}}
							<h3>Ocupación</h3>

							<fieldset class="form-input">
								{{-- Aca se agregan con javascript las solapas enteras dependiendo si selecciono empresa o usuario --}}
							</fieldset>
						</form>			
					</div>
				</div>			
			</div>
		</div>
	</div>

	<!-- jquery latest version -->
	<script src="{{asset('assets/registro/js/jquery.min.js')}}"></script>
	<!-- popper.min.js -->
	<script src="{{asset('assets/registro/js/popper.min.js')}}"></script>    
	<!-- bootstrap js -->
	<script src="{{asset('assets/registro/js/bootstrap.min.js')}}"></script>
	<!-- jquery.steps js -->
	<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
	<script src="{{asset('assets/registro/js/jquery.steps.js')}}"></script>
	<!-- particles js -->
	<script src="{{asset('assets/registro/js/particles.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			particlesJS("particles-js", 
				{
				"particles": {
					"number": {
					"value": 160,
					"density": {
						"enable": true,
						"value_area": 800
					}
					},
					"color": {
					"value": "#ffffff"
					},
					"shape": {
					"type": "circle",
					"stroke": {
						"width": 0,
						"color": "#000000"
					},
					"polygon": {
						"nb_sides": 5
					},
					"image": {
						"src": "{{asset('assets/registro/img/github.svg')}}",
						"width": 100,
						"height": 100
					}
					},
					"opacity": {
					"value": 1,
					"random": true,
					"anim": {
						"enable": true,
						"speed": 1,
						"opacity_min": 0,
						"sync": false
					}
					},
					"size": {
					"value": 3,
					"random": true,
					"anim": {
						"enable": false,
						"speed": 4,
						"size_min": 0.3,
						"sync": false
					}
					},
					"line_linked": {
					"enable": false,
					"distance": 150,
					"color": "#ffffff",
					"opacity": 0.4,
					"width": 1
					},
					"move": {
					"enable": true,
					"speed": 1,
					"direction": "none",
					"random": true,
					"straight": false,
					"out_mode": "out",
					"bounce": false,
					"attract": {
						"enable": false,
						"rotateX": 600,
						"rotateY": 600
					}
					}
				},
				"interactivity": {
					"detect_on": "canvas",
					"events": {
					"onhover": {
						"enable": true,
						"mode": "bubble"
					},
					"onclick": {
						"enable": true,
						"mode": "repulse"
					},
					"resize": true
					},
					"modes": {
					"grab": {
						"distance": 400,
						"line_linked": {
						"opacity": 1
						}
					},
					"bubble": {
						"distance": 250,
						"size": 0,
						"duration": 2,
						"opacity": 0,
						"speed": 3
					},
					"repulse": {
						"distance": 400,
						"duration": 0.4
					},
					"push": {
						"particles_nb": 4
					},
					"remove": {
						"particles_nb": 2
					}
					}
				},
				"retina_detect": true
				}
			);
		});
	</script>

	<script>
		var form = $("#example-advanced-form").show();

		form.steps({
			headerTag: "h3",
			bodyTag: "fieldset",
			transitionEffect: "slideLeft",
			onStepChanging: function (event, currentIndex, newIndex)
			{
				// Allways allow previous action even if the current form is not valid!
				if (currentIndex > newIndex)
				{
					return true;
				}
				// Forbid next action on "Warning" step if the user is to young
				if (newIndex === 3 && Number($("#age").val()) < 18)
				{
					return false;
				}
				// Needed in some cases if the user went back (clean up)
				if (currentIndex < newIndex)
				{
					// To remove error styles
					form.find(".body:eq(" + newIndex + ") label.error").remove();
					form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
				}
				form.validate().settings.ignore = ":disabled,:hidden";
				return form.valid();
			},
			onStepChanged: function (event, currentIndex, priorIndex)
			{
				// Used to skip the "Warning" step if the user is old enough.
				if (currentIndex === 2 && Number($("#age").val()) >= 18)
				{
					form.steps("next");
				}
				// Used to skip the "Warning" step if the user is old enough and wants to the previous step.
				if (currentIndex === 2 && priorIndex === 3)
				{
					form.steps("previous");
				}
			},
			onFinishing: function (event, currentIndex)
			{
				form.validate().settings.ignore = ":disabled";
				return form.valid();
			},
			onFinished: function (event, currentIndex)
			{
				$("#example-advanced-form").submit();
			}
		}).validate({
			errorPlacement: function errorPlacement(error, element) { element.before(error); },
			rules: {
				confirm: {
					equalTo: "#password"
				}
			}
		});
	</script>

	<script>
		$( document ).ready(function(){
			$("input[type=submit]").on('click',function(){
				if(this.value=='Registrar'){
					valida();
				}
			});
		});
		function valida(){
			if($("#pn").val()==""){
				alertify.error("Primer Nombre es Obligatorio");
			}
			if($("#cel1").val()==""){
				alertify.error("Celular Obligatorio");
			}
			if($("#dir").val()==""){
				alertify.error("Dirección Obligatorio");
			}
			if($("#nd").val()==""){
				alertify.error("Numero de Documento es Obligatorio");
			}
		}
		function aviso() {
			alertify.confirm("Saltar Registro","<p>Por el momento solo tenemos tu Correo Electrónico, debes llenar todo el formulario para poder realizar alguna transaccion, no se te permitira realizar movimientos ni registrar ninguna cuenta.</p> <p>Y tampoco guardamos el avance del registro.</p> <h4><strong>¿Esta seguro de hacerlo en otro momento?</strong></h4>",
				function(){
					window.location="{{url('home')}}";
				},
				function(){
					alertify.success("Por favor, Continua con el registro");
				}
			);
		}
	</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/AlertifyJS/1.11.4/alertify.min.js"></script>
<script>
    //==========================================================
    // TODO: esto iria al lado de los datos del perfil del usuario para cambiar de perfil activo
    $( document ).ready(function(){
        $.ajax({
            url: '{{route("getPerfiles")}}',
            type: 'get',
            dataType: 'json',

            beforeSend: function(){
            },
            success: function(response){
                if(response['cant_perfiles_usuario_personal'] > 0){
                    $('#perfiles').append('<option disabled>Perfiles personales</option>');
                    $.each( response['perfiles_usuario_personal'], function( key, value ) {
                        $('#perfiles').append('<option value="'+value["id_usuario_personal"]+'" data-id="0">'+value["primer_nombre"]+' | '+value["primer_apellido"]+'</option>');
                    });
                    if(response['tipo_perfil_activo'] == 0){
                        $('#perfiles option[value="'+response['perfil_activo']+'"]').attr("selected", "selected");
                    }
                }

                if(response['cant_perfiles_usuario_empresa'] > 0){
                    $('#perfiles').append('<option disabled>Perfiles de empresas</option>');
                    $.each( response['perfiles_usuario_empresa'], function( key, value ) {
                        $('#perfiles').append('<option value="'+value["id_usuario_empresa"]+'" data-id="1">'+value["ruc"]+' | '+value["razon_social"]+'</option>');
                    });

                    if(response['tipo_perfil_activo'] == 1){
                        $('#perfiles option[value="'+response['perfil_activo']+'"]').attr("selected", "selected");
                    }
                }
            },
            complete:function(data){
            }
        });
    });

    $("#perfiles").on("change", function(){
        var token = '{{csrf_token()}}';

        let tipo_perfil_activo = $(this).find(':selected').attr('data-id');
        let perfil_activo = $(this).children("option:selected").val();

        $.ajax({
            url: '{{route("changePerfilActivo")}}',
            type: 'post',
            data: {
                tipo_perfil_activo: tipo_perfil_activo,
                perfil_activo: perfil_activo,
                _token: token
            },
            dataType: 'json',

            beforeSend: function(){
                // Show image container
                $('#div_carga').fadeIn();
            },
            success: function(response){
                if(response['success']){
                    alertify.success(response['message']);
                }
                else{
                    alertify.error(response['message']);
                }
            },
            complete:function(data){
                window.setTimeout(function(){location.reload()},1000)
            }
        });
    });

    //==========================================================

	$(".persona_o_empresa").on("click", function(){
		$("#persona_o_empresa_selected").val("ok");
		var id_boton = this.id;

		if(id_boton === "persona"){

			$("#example-advanced-form-t-2").text("3. Domicilio");
			$("#example-advanced-form-t-3").text("4. Ocupación");

			$("#example-advanced-form").attr('action', "{{route('createPerfilPersonal')}}");

			let solapa_datos = crearSolapaDatos_usuario();
			$("#example-advanced-form-p-1").empty();
			$("#example-advanced-form-p-1").html(solapa_datos);

			//Consulto departamentos, provincias y distritos llamando a ese get y despues las funciones en cadena.
			$.get("{{url('api/departamento')}}",function(ls){
				for (var i =  1; i < ls.length; i++) {
					$("#id_departamento").append('<option value="'+ls[i].dDepartamento+'">'+ls[i].Descripcion+'</option>');
				}
				//DEJO SELECCIONADA LA OPCION DE LIMA
				$('#id_departamento option[value="15"]').attr("selected", "selected");
				provincia($("#id_departamento").val());
			});

			let solapa_domicilio = crearSolapaDomicilio_usuario();
			$("#example-advanced-form-p-2").empty();
			$("#example-advanced-form-p-2").html(solapa_domicilio);

			let solapa_ocupacion = crearSolapaOcupacion_usuario();
			$("#example-advanced-form-p-3").empty();
			$("#example-advanced-form-p-3").html(solapa_ocupacion);
		}
		else if (id_boton === "empresa"){

			$("#example-advanced-form-t-2").text("3. Legal");
			$("#example-advanced-form-t-3").text("4. Contacto");

			$("#example-advanced-form").attr('action', "{{route('createPerfilEmpresa')}}");

			let solapa_datos = crearSolapaDatos_empresa();
			$("#example-advanced-form-p-1").empty();
			$("#example-advanced-form-p-1").html(solapa_datos);

			//Consulto departamentos, provincias y distritos llamando a ese get y despues las funciones en cadena.
			$.get("{{url('api/departamento')}}",function(ls){
				for (var i =  1; i < ls.length; i++) {
					$("#id_departamento").append('<option value="'+ls[i].dDepartamento+'">'+ls[i].Descripcion+'</option>');
				}
				//DEJO SELECCIONADA LA OPCION DE LIMA
				$('#id_departamento option[value="15"]').attr("selected", "selected");
				provincia($("#id_departamento").val());
			});

			let solapa_legal = crearSolapaLegal_empresa();
			$("#example-advanced-form-p-2").empty();
			$("#example-advanced-form-p-2").html(solapa_legal);

			let solapa_contacto = crearSolapaContacto_empresa();
			$("#example-advanced-form-p-3").empty();
			$("#example-advanced-form-p-3").html(solapa_contacto);

		}
	});

	$.get("{{url('api/departamento')}}",function(ls){
		for (var i =  1; i < ls.length; i++) {
			$("#id_departamento").append('<option value="'+ls[i].dDepartamento+'">'+ls[i].Descripcion+'</option>');
		}
		provincia($("#id_departamento").val());
	});
	function provincia(i) {
		$("#id_provincia option").remove();
		$.get("{{url('api/provincia')}}/"+i,function(ls){
			for (var i =  1; i < ls.length; i++) {
				$("#id_provincia").append('<option value="'+ls[i].codProvincia+'">'+ls[i].Descripcion+'</option>');
			}
			//DEJO SELECCIONADA LA OPCION DE LIMA
			$('#id_provincia option[value="0"]').attr("selected", "selected");
			distrito($("#id_departamento").val(),$("#id_provincia").val());
		});
	}
	function distrito(i,j) {
		$("#id_distrito option").remove();
		$.get("{{url('api/distrito')}}/"+i+"/"+j,function(ls){
			for (var i =  1; i < ls.length; i++) {
				$("#id_distrito").append('<option value="'+ls[i].codDistrito+'">'+ls[i].Descripcion+'</option>');
			}
		});
	}
	$(document).on("change", "#id_departamento", function() {
		provincia($("#id_departamento").val());
	});
	$(document).on("change", "#id_provincia", function() {
		distrito($("#id_departamento").val(),$("#id_provincia").val());
	});

	// ================================= Solapas para usuario empresa =================================
	function crearSolapaDatos_empresa(){
		return '<center>'+
					'<h4>Datos de la empresa</h4>'+
				'</center>'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Número de RUC</label>'+
									'<input type="text" class="form-control" name="ruc" id="ruc" placeholder="Número de RUC" required>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Razón social</label>'+
									'<input type="text" class="form-control" name="razon_social" id="razon_social" placeholder="Razón Social">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Giro del negocio</label>'+
									'<input type="text" class="form-control" name="giro_negocio" id="giro_negocio" placeholder="Giro del negocio">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Dirección Fiscal</label>'+
									'<input type="text" class="form-control" name="direccion_fiscal" id="direccion_fiscal" placeholder="Dirección">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Pais</label>'+
									'<select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_pais" id="select-countries">'+
										'<option label="Elija uno">'+
										'</option>'+
											'@foreach($pas as $pa)'+
												'<option value="{{$pa->id_pais}}">{{$pa->nombre}}</option>'+
											'@endforeach'+
									'</select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Departamento</label>'+
									'<select class="form-control select2 custom-select" name="id_departamento" id="id_departamento" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Provincia</label>'+
									'<select class="form-control select2 custom-select" name="id_provincia" id="id_provincia" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Distrito</label>'+
									'<select class="form-control select2 custom-select" name="id_distrito" id="id_distrito" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Correo Electrónico</label>'+
									'<input type="text" class="form-control" name="email" id="email" maxlength="35" placeholder="Correo Electrónico">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Teléfono</label>'+
									'<input type="text" class="form-control" name="telefono" id="telefono" maxlength="50" placeholder="Teléfono">'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}

	function crearSolapaLegal_empresa(){
		return '<center>'+
					'<h4>Datos del Representante Legal</h4>'+
				'</center>'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Primer nombre</label>'+
									'<input type="text" class="form-control" name="primer_nombre_rp" id="primer_nombre_rp" placeholder="Primer nombre">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Segundo nombre</label>'+
									'<input type="text" class="form-control" name="segundo_nombre_rp" id="segundo_nombre_rp"  placeholder="Segundo nombre">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Primer Apellido</label>'+
									'<input type="text" class="form-control" name="primer_apellido_rp" id="primer_apellido_rp" placeholder="Primero Apellido">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Segundo Apellido</label>'+
									'<input type="text" class="form-control" name="segundo_apellido_rp" id="segundo_apellido_rp" placeholder="Segundo Apellido">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Tipo de documento</label>'+
									'<select class="form-control select2 custom-select" name="id_tipo_documento_rp" id="id_tipo_documento_rp" data-placeholder="Elija uno">'+
										'@foreach($doc as $do)'+
											'<option value="{{$do->id_tipo_documento}}">{{$do->nombre}}</option>'+
										'@endforeach'+
									'</select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Nùmero de documento</label>'+
									'<input type="text" class="form-control" name="numero_documento_rp" id="numero_documento_rp" placeholder="Número de documento" required>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}

	function crearSolapaContacto_empresa(){
		return '<center>'+
					'<h4>Datos del Contacto</h4>'+
				'</center>	'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Primer nombre</label>'+
									'<input type="text" class="form-control" name="primer_nombre_c" id="primer_nombre_c" placeholder="Primer nombre">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Segundo nombre</label>'+
									'<input type="text" class="form-control" name="segundo_nombre_c" id="segundo_nombre_c" placeholder="Segundo nombre">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Primer Apellido</label>'+
									'<input type="text" class="form-control" name="primer_apellido_c" id="primer_apellido_c" placeholder="Primero Apellido">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Segundo Apellido</label>'+
									'<input type="text" class="form-control" name="asegundo_apellido_c" id="segundo_apellido_c" placeholder="Segundo Apellido">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Tipo de documento</label>'+
									'<select class="form-control select2 custom-select" name="id_tipo_documento_c" id="id_tipo_documento_c" data-placeholder="Elija uno">'+
										'@foreach($doc as $do)'+
											'<option value="{{$do->id_tipo_documento}}">{{$do->nombre}}</option>'+
										'@endforeach'+
									'</select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Nùmero de documento</label>'+
									'<input type="text" class="form-control" name="numero_documento_c" id="numero_documento_c" placeholder="Número de documento">'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Télefono</label>'+
									'<input type="text" class="form-control" name="telefono_c" id="telefono_c" maxlength="35" placeholder="Télefono" required>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}
	// ================================================================================================



	// ===================================== Solapas para usuario =====================================
	function crearSolapaDatos_usuario(){
		return '<center>'+
					'<h4>Datos personales</h4>'+
				'</center>'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Primer nombre</label>'+
										'<input type="text" class="form-control" name="primer_nombre" id="primer_nombre" placeholder="Primer nombre">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Segundo nombre</label>'+
										'<input type="text" class="form-control" name="segundo_nombre" id="segundo_nombre" placeholder="Segundo nombre">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Primer Apellido</label>'+
										'<input type="text" class="form-control" name="primer_apellido" id="primer_apellido" placeholder="Primero Apellido">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Segundo Apellido</label>'+
										'<input type="text" class="form-control" name="segundo_apellido" id="segundo_apellido" placeholder="Segundo Apellido">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Tipo de documento</label>'+
										'<select class="form-control select2 custom-select" name="id_tipo_documento" id="id_tipo_documento" data-placeholder="Elija uno">'+
											'@foreach($doc as $do)'+
												'<option value="{{$do->id_tipo_documento}}">{{$do->nombre}}</option>'+
											'@endforeach'+
										'</select>'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Número de documento</label>'+
										'<input type="text" class="form-control" name="numero_documento" id="numero_documento" placeholder="Número de documento" required>'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Fecha de Nacimiento</label>'+
										'<input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" value="{{date('Y-m-d')}}">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Teléfono</label>'+
										'<input type="text" class="form-control" name="telefono" id="telefono" placeholder="Teléfono">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Celular</label>'+
										'<input type="text" class="form-control" name="celular1" id="celular1" placeholder="Celular">'+
									'</div>'+
									'<div class="form-group col-md-6">'+
										'<label class="form-label">Celular 2</label>'+
										'<input type="text" class="form-control" name="celular2" id="celular2" placeholder="Otro celular">'+
									'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}

	function crearSolapaDomicilio_usuario(){
		return '<center>'+
					'<h4>Datos del domicilio</h4>'+
				'</center>	'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Pais</label>'+
									'<select class="form-control select2 custom-select" data-placeholder="Elija uno" name="id_pais" id="select-countries">'+
										'<option label="Elija uno">'+
										'</option>'+
											'@foreach($pas as $pa)'+
												'<option value="{{$pa->id_pais}}">{{$pa->nombre}}</option>'+
											'@endforeach'+
									'</select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Departamento</label>'+
									'<select class="form-control select2 custom-select" name="id_departamento" id="id_departamento" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Provincia</label>'+
									'<select class="form-control select2 custom-select" name="id_provincia" id="id_provincia" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-6">'+
									'<label class="form-label">Distrito</label>'+
									'<select class="form-control select2 custom-select" name="id_distrito" id="id_distrito" data-placeholder="Elija uno"></select>'+
								'</div>'+
								'<div class="form-group col-md-12">'+
									'<label class="form-label">Dirección</label>'+
									'<input type="text" class="form-control" name="direccion" id="direccion" placeholder="Dirección">'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}

	function crearSolapaOcupacion_usuario(){
		return '<center>'+
					'<h4>Ocupación</h4>'+
				'</center>'+
				'<div class="col-md-12">'+
					'<div class="card">'+
						'<div class="card-body">'+
							'<div class="row">'+
								'<div class="form-group col-md-12">'+
									'<label class="form-label">Ocupación</label>'+
									'<select class="form-control select2 custom-select" name="id_ocupacion" id="id_ocupacion" data-placeholder="Elija uno">'+
										'@foreach($ocs as $oc)'+
											'<option value="{{$oc->id_ocupacion}}">{{$oc->nombre}}</option>'+
										'@endforeach'+
									'</select>'+
								'</div>'+
								'<label class="form-label">¿Es usted una persona expuesta políticamente?</label>'+
								'<div class="form-group col-md-12">'+
									'<label class="custom-control custom-radio">'+
										'<input required type="radio" class="custom-control-input required" name="persona_expuesta" value="1" checked>'+
										'<span class="custom-control-label">Si</span>'+
									'</label>'+
									'<br>'+
									'<label class="custom-control custom-radio">'+
										'<input required type="radio" class="custom-control-input required" name="persona_expuesta" value="0">'+
										'<span class="custom-control-label">No</span>'+
									'</label>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
	}
	// ================================================================================================
</script>

<script>
	jQuery.extend(jQuery.validator.messages, {
		required: "Este campo es requerido.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date (ISO).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		accept: "Please enter a value with a valid extension.",
		maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
		minlength: jQuery.validator.format("Please enter at least {0} characters."),
		rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
		range: jQuery.validator.format("Please enter a value between {0} and {1}."),
		max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
		min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
	});
</script>

</body>
</html>